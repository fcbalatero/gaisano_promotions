MANIFEST.in
README.md
license.txt
requirements.txt
setup.py
gaisano_promotions/__init__.py
gaisano_promotions/create_barcode.py
gaisano_promotions/hooks.py
gaisano_promotions/modules.txt
gaisano_promotions/patches.txt
gaisano_promotions/test.py
gaisano_promotions.egg-info/PKG-INFO
gaisano_promotions.egg-info/SOURCES.txt
gaisano_promotions.egg-info/dependency_links.txt
gaisano_promotions.egg-info/not-zip-safe
gaisano_promotions.egg-info/requires.txt
gaisano_promotions.egg-info/top_level.txt
gaisano_promotions/config/__init__.py
gaisano_promotions/config/desktop.py
gaisano_promotions/config/docs.py
gaisano_promotions/gaisano_promotions/__init__.py
gaisano_promotions/gaisano_promotions/doctype/__init__.py
gaisano_promotions/gaisano_promotions/doctype/barcode_label/__init__.py
gaisano_promotions/gaisano_promotions/doctype/barcode_label/barcode_label.js
gaisano_promotions/gaisano_promotions/doctype/barcode_label/barcode_label.json
gaisano_promotions/gaisano_promotions/doctype/barcode_label/barcode_label.py
gaisano_promotions/gaisano_promotions/doctype/barcode_label/test_barcode_label.js
gaisano_promotions/gaisano_promotions/doctype/barcode_label/test_barcode_label.py
gaisano_promotions/gaisano_promotions/doctype/barcode_printing/__init__.py
gaisano_promotions/gaisano_promotions/doctype/barcode_printing/barcode_printing.js
gaisano_promotions/gaisano_promotions/doctype/barcode_printing/barcode_printing.json
gaisano_promotions/gaisano_promotions/doctype/barcode_printing/barcode_printing.py
gaisano_promotions/gaisano_promotions/doctype/barcode_printing/test_barcode_printing.js
gaisano_promotions/gaisano_promotions/doctype/barcode_printing/test_barcode_printing.py
gaisano_promotions/gaisano_promotions/doctype/cellophane/__init__.py
gaisano_promotions/gaisano_promotions/doctype/cellophane/cellophane.js
gaisano_promotions/gaisano_promotions/doctype/cellophane/cellophane.json
gaisano_promotions/gaisano_promotions/doctype/cellophane/cellophane.py
gaisano_promotions/gaisano_promotions/doctype/cellophane/test_cellophane.js
gaisano_promotions/gaisano_promotions/doctype/cellophane/test_cellophane.py
gaisano_promotions/gaisano_promotions/doctype/generate_barcodes/__init__.py
gaisano_promotions/gaisano_promotions/doctype/generate_barcodes/generate_barcodes.js
gaisano_promotions/gaisano_promotions/doctype/generate_barcodes/generate_barcodes.json
gaisano_promotions/gaisano_promotions/doctype/generate_barcodes/generate_barcodes.py
gaisano_promotions/gaisano_promotions/doctype/generate_barcodes/test_generate_barcodes.js
gaisano_promotions/gaisano_promotions/doctype/generate_barcodes/test_generate_barcodes.py
gaisano_promotions/gaisano_promotions/doctype/other_items/__init__.py
gaisano_promotions/gaisano_promotions/doctype/other_items/other_items.js
gaisano_promotions/gaisano_promotions/doctype/other_items/other_items.json
gaisano_promotions/gaisano_promotions/doctype/other_items/other_items.py
gaisano_promotions/gaisano_promotions/doctype/other_items/test_other_items.js
gaisano_promotions/gaisano_promotions/doctype/other_items/test_other_items.py
gaisano_promotions/gaisano_promotions/doctype/other_items_table/__init__.py
gaisano_promotions/gaisano_promotions/doctype/other_items_table/other_items_table.json
gaisano_promotions/gaisano_promotions/doctype/other_items_table/other_items_table.py
gaisano_promotions/gaisano_promotions/doctype/print_barcode/__init__.py
gaisano_promotions/gaisano_promotions/doctype/print_barcode/print_barcode.js
gaisano_promotions/gaisano_promotions/doctype/print_barcode/print_barcode.json
gaisano_promotions/gaisano_promotions/doctype/print_barcode/print_barcode.py
gaisano_promotions/gaisano_promotions/doctype/print_barcode/test_print_barcode.js
gaisano_promotions/gaisano_promotions/doctype/print_barcode/test_print_barcode.py
gaisano_promotions/gaisano_promotions/doctype/production_costs/__init__.py
gaisano_promotions/gaisano_promotions/doctype/production_costs/production_costs.js
gaisano_promotions/gaisano_promotions/doctype/production_costs/production_costs.json
gaisano_promotions/gaisano_promotions/doctype/production_costs/production_costs.py
gaisano_promotions/gaisano_promotions/doctype/production_costs/test_production_costs.js
gaisano_promotions/gaisano_promotions/doctype/production_costs/test_production_costs.py
gaisano_promotions/gaisano_promotions/doctype/promo_bundle_items/__init__.py
gaisano_promotions/gaisano_promotions/doctype/promo_bundle_items/promo_bundle_items.js
gaisano_promotions/gaisano_promotions/doctype/promo_bundle_items/promo_bundle_items.json
gaisano_promotions/gaisano_promotions/doctype/promo_bundle_items/promo_bundle_items.py
gaisano_promotions/gaisano_promotions/doctype/promo_bundle_items/test_promo_bundle_items.js
gaisano_promotions/gaisano_promotions/doctype/promo_bundle_items/test_promo_bundle_items.py
gaisano_promotions/gaisano_promotions/doctype/promo_items/__init__.py
gaisano_promotions/gaisano_promotions/doctype/promo_items/promo_items.js
gaisano_promotions/gaisano_promotions/doctype/promo_items/promo_items.json
gaisano_promotions/gaisano_promotions/doctype/promo_items/promo_items.py
gaisano_promotions/gaisano_promotions/doctype/promo_items/test_promo_items.js
gaisano_promotions/gaisano_promotions/doctype/promo_items/test_promo_items.py
gaisano_promotions/gaisano_promotions/doctype/promo_production/__init__.py
gaisano_promotions/gaisano_promotions/doctype/promo_production/promo_production.js
gaisano_promotions/gaisano_promotions/doctype/promo_production/promo_production.json
gaisano_promotions/gaisano_promotions/doctype/promo_production/promo_production.py
gaisano_promotions/gaisano_promotions/doctype/promo_production/test_promo_production.js
gaisano_promotions/gaisano_promotions/doctype/promo_production/test_promo_production.py
gaisano_promotions/gaisano_promotions/doctype/promo_production_costs/__init__.py
gaisano_promotions/gaisano_promotions/doctype/promo_production_costs/promo_production_costs.json
gaisano_promotions/gaisano_promotions/doctype/promo_production_costs/promo_production_costs.py
gaisano_promotions/gaisano_promotions/doctype/promo_production_item/__init__.py
gaisano_promotions/gaisano_promotions/doctype/promo_production_item/promo_production_item.json
gaisano_promotions/gaisano_promotions/doctype/promo_production_item/promo_production_item.py
gaisano_promotions/gaisano_promotions/doctype/promo_production_operations/__init__.py
gaisano_promotions/gaisano_promotions/doctype/promo_production_operations/promo_production_operations.json
gaisano_promotions/gaisano_promotions/doctype/promo_production_operations/promo_production_operations.py
gaisano_promotions/gaisano_promotions/doctype/promotions/__init__.py
gaisano_promotions/gaisano_promotions/doctype/promotions/promotions.js
gaisano_promotions/gaisano_promotions/doctype/promotions/promotions.json
gaisano_promotions/gaisano_promotions/doctype/promotions/promotions.py
gaisano_promotions/gaisano_promotions/doctype/promotions/promotions_dashboard.py
gaisano_promotions/gaisano_promotions/doctype/promotions/test_promotions.js
gaisano_promotions/gaisano_promotions/doctype/promotions/test_promotions.py
gaisano_promotions/gaisano_promotions/doctype/promotions_budget/__init__.py
gaisano_promotions/gaisano_promotions/doctype/promotions_budget/promotions_budget.js
gaisano_promotions/gaisano_promotions/doctype/promotions_budget/promotions_budget.json
gaisano_promotions/gaisano_promotions/doctype/promotions_budget/promotions_budget.py
gaisano_promotions/gaisano_promotions/doctype/promotions_budget/promotions_budget_dashboard.py
gaisano_promotions/gaisano_promotions/doctype/promotions_budget/test_promotions_budget.js
gaisano_promotions/gaisano_promotions/doctype/promotions_budget/test_promotions_budget.py
gaisano_promotions/gaisano_promotions/doctype/promotions_calculation/__init__.py
gaisano_promotions/gaisano_promotions/doctype/promotions_calculation/promotions_calculation.js
gaisano_promotions/gaisano_promotions/doctype/promotions_calculation/promotions_calculation.json
gaisano_promotions/gaisano_promotions/doctype/promotions_calculation/promotions_calculation.py
gaisano_promotions/gaisano_promotions/doctype/promotions_calculation/test_promotions_calculation.js
gaisano_promotions/gaisano_promotions/doctype/promotions_calculation/test_promotions_calculation.py
gaisano_promotions/gaisano_promotions/doctype/promotions_calculation_operations/__init__.py
gaisano_promotions/gaisano_promotions/doctype/promotions_calculation_operations/promotions_calculation_operations.json
gaisano_promotions/gaisano_promotions/doctype/promotions_calculation_operations/promotions_calculation_operations.py
gaisano_promotions/gaisano_promotions/doctype/promotions_calculation_table/__init__.py
gaisano_promotions/gaisano_promotions/doctype/promotions_calculation_table/promotions_calculation_table.js
gaisano_promotions/gaisano_promotions/doctype/promotions_calculation_table/promotions_calculation_table.json
gaisano_promotions/gaisano_promotions/doctype/promotions_calculation_table/promotions_calculation_table.py
gaisano_promotions/gaisano_promotions/doctype/promotions_calculation_table/test_promotions_calculation_table.js
gaisano_promotions/gaisano_promotions/doctype/promotions_calculation_table/test_promotions_calculation_table.py
gaisano_promotions/gaisano_promotions/doctype/promotions_other_items/__init__.py
gaisano_promotions/gaisano_promotions/doctype/promotions_other_items/promotions_other_items.json
gaisano_promotions/gaisano_promotions/doctype/promotions_other_items/promotions_other_items.py
gaisano_promotions/gaisano_promotions/doctype/regular_items/__init__.py
gaisano_promotions/gaisano_promotions/doctype/regular_items/regular_items.json
gaisano_promotions/gaisano_promotions/doctype/regular_items/regular_items.py
gaisano_promotions/gaisano_promotions/doctype/scotch_tape/__init__.py
gaisano_promotions/gaisano_promotions/doctype/scotch_tape/scotch_tape.js
gaisano_promotions/gaisano_promotions/doctype/scotch_tape/scotch_tape.json
gaisano_promotions/gaisano_promotions/doctype/scotch_tape/scotch_tape.py
gaisano_promotions/gaisano_promotions/doctype/scotch_tape/test_scotch_tape.js
gaisano_promotions/gaisano_promotions/doctype/scotch_tape/test_scotch_tape.py
gaisano_promotions/gaisano_promotions/print_format/__init__.py
gaisano_promotions/gaisano_promotions/print_format/print_barcode_format/__init__.py
gaisano_promotions/gaisano_promotions/print_format/print_barcode_format/print_barcode_format.json
gaisano_promotions/gaisano_promotions/print_format/promo_production/__init__.py
gaisano_promotions/gaisano_promotions/print_format/promo_production/promo_production.json
gaisano_promotions/gaisano_promotions/print_format/promotions/__init__.py
gaisano_promotions/gaisano_promotions/print_format/promotions/promotions.json
gaisano_promotions/gaisano_promotions/report/__init__.py
gaisano_promotions/gaisano_promotions/report/barcode_control/__init__.py
gaisano_promotions/gaisano_promotions/report/barcode_control/barcode_control.html
gaisano_promotions/gaisano_promotions/report/barcode_control/barcode_control.js
gaisano_promotions/gaisano_promotions/report/barcode_control/barcode_control.json
gaisano_promotions/gaisano_promotions/report/barcode_control/barcode_control.py
gaisano_promotions/gaisano_promotions/report/barcode_printing/__init__.py
gaisano_promotions/gaisano_promotions/report/barcode_printing/barcode_printing.html
gaisano_promotions/gaisano_promotions/report/barcode_printing/barcode_printing.js
gaisano_promotions/gaisano_promotions/report/barcode_printing/barcode_printing.json
gaisano_promotions/gaisano_promotions/report/barcode_printing/barcode_printing.py
gaisano_promotions/gaisano_promotions/report/promotions_report/__init__.py
gaisano_promotions/gaisano_promotions/report/promotions_report/promotions_report.js
gaisano_promotions/gaisano_promotions/report/promotions_report/promotions_report.json
gaisano_promotions/gaisano_promotions/report/promotions_report/promotions_report.py
gaisano_promotions/gaisano_promotions/report/promotions_report_per_dm/__init__.py
gaisano_promotions/gaisano_promotions/report/promotions_report_per_dm/promotions_report_per_dm.js
gaisano_promotions/gaisano_promotions/report/promotions_report_per_dm/promotions_report_per_dm.json
gaisano_promotions/gaisano_promotions/report/promotions_report_per_dm/promotions_report_per_dm.py
gaisano_promotions/gaisano_promotions/report/total_promotions_per_supplier/__init__.py
gaisano_promotions/gaisano_promotions/report/total_promotions_per_supplier/total_promotions_per_supplier.js
gaisano_promotions/gaisano_promotions/report/total_promotions_per_supplier/total_promotions_per_supplier.json
gaisano_promotions/gaisano_promotions/report/total_promotions_per_supplier/total_promotions_per_supplier.py
gaisano_promotions/print_formats/backup.html
gaisano_promotions/print_formats/print_barcode_format.html
gaisano_promotions/print_formats/promo_production.html
gaisano_promotions/print_formats/promotions.html
gaisano_promotions/print_formats/promotions_calculations.html
gaisano_promotions/print_formats/promo_production/promo_production.html
gaisano_promotions/templates/__init__.py
gaisano_promotions/templates/pages/__init__.py