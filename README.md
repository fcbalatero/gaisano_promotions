## Gaisano Promotions

Gaisano Promotions

#### License

MIT

####Installation instructions

Create Promotions User and Promotions Manager in database:
```
    bench mysql
    INSERT INTO `tabRole` (name, creation, modified, modified_by, owner, role_name, desk_access) VALUES ('Promotions User', NOW(), NOW(), 'Administrator', 'Administrator', 'Promotions User', 1);
    INSERT INTO `tabRole` (name, creation, modified, modified_by, owner, role_name, desk_access) VALUES ('Promotions Manager', NOW(), NOW(), 'Administrator', 'Administrator', 'Promotions Manager', 1);
```

Go to `frappe-bench-folder/env/bin` and `source activate`
install pybarcode using `pip install pybarcode`

install the app using regular installation procedures

####updated 2021-11-02