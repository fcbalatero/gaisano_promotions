import frappe
from gaisano.events import create_barcode

@frappe.whitelist()
def test():
    material_receipt_doc = frappe.get_doc({
        "doctype": "Role",
        "role_name": "Test Role 1",
    })
    try:
        material_receipt_doc.insert(ignore_permissions=True)

        material_receipt_doc.submit()
        print("NAAAAAAAAAAAAAAAAMMME")
        print(material_receipt_doc.name)
    except Exception:
        print(frappe.get_traceback())

    frappe.db.sql(""" DELETE FROM `tabRole` WHERE name= %s""",material_receipt_doc.name)



@frappe.whitelist()
def test_save():
    data = frappe.get_doc("Promo Production", "PROD-MALL-000503")
    data.save()

@frappe.whitelist()
def change_item_cost():
    promotions_data = frappe.db.sql(""" SELECT * FROM `tabPromotions`""", as_dict=True)
    for i in promotions_data:
        if not i.use_existing_bundle:
            smallest_item_cost = ""
            for ii in i.item_table:
                item_data_for_item_cost = frappe.db.sql(""" SELECT * FROM `tabItem` WHERE name=%s """, (ii.promo_item), as_dict=True)
                if smallest_item_cost:
                    if float(str(item_data_for_item_cost[0].actual_retail_rate).replace("%", " ")) < smallest_item_cost:
                        smallest_item_cost = float(str(item_data_for_item_cost[0].actual_retail_rate).replace("%", " "))
                else:
                    smallest_item_cost = float(str(item_data_for_item_cost[0].actual_retail_rate).replace("%", " "))

            item_cost = float(i.srp_of_bundle) / float(float(smallest_item_cost)/100 + 1)
            frappe.db.sql(""" UPDATE `tabItem` set item_cost=%s WHERE item_cost=%s and type=%s and item_name_dummy=%s and item_price_retail_with_margin=%s""",(i.srp_of_bundle,item_cost,"Company Bundling", i.new_item_name, i.srp_of_bundle))


def check():
    smallest_item_cost = ""
    item_data_for_item_cost = frappe.db.sql(""" SELECT * FROM `tabItem` WHERE name=%s """,("ITEM-076278"), as_dict=True)
    if smallest_item_cost:
        if float(str(item_data_for_item_cost[0].actual_retail_rate).replace("%", " ")) < float(smallest_item_cost.replace("%", " ")):
            smallest_item_cost = float(str(item_data_for_item_cost[0].actual_retail_rate).replace("%", " "))
    else:
        smallest_item_cost = float(str(item_data_for_item_cost[0].actual_retail_rate).replace("%", " "))
    print("SMALLEST")
    print(smallest_item_cost)

def create_stock_entry(prod_name):
    promo_prod = frappe.db.sql(""" SELECT * FROM `tabPromo Production` WHERE name=%s """,(prod_name), as_dict=True)
    warehouse_default_promo = frappe.db.sql(""" SELECT name FROM `tabWarehouse` WHERE name LIKE %s """, ("Promo%"),
                                            as_dict=True)

    item_name_in_promotions = frappe.db.sql(""" SELECT * FROM `tabPromotions` WHERE name=%s """, (promo_prod[0].promo_name),
                                            as_dict=True)
    item_code_new_item = frappe.db.sql(""" SELECT * FROM `tabItem` WHERE item_name_dummy=%s """,
                                       (item_name_in_promotions[0].new_item_name), as_dict=True)
    promo_budget_branch_code = frappe.db.sql(""" SELECT branch_code FROM `tabPromotions Budget` WHERE name=%s """,
                                             (promo_prod[0].debit_memo), as_dict=True)
    material_receipt_doc = frappe.get_doc({
        "doctype": "Stock Entry",
        "type": "Stock Transfer",
        "branch": promo_budget_branch_code[0].branch_code,
        "naming_series": "PROMO-." + promo_prod[0].branch_code + ".-.YY.MM.DD",
    })
    material_receipt_doc.append("items", {
        "s_warehouse": warehouse_default_promo[0].name,
        "t_warehouse": promo_prod[0].target_warehouse,
        "item_code": item_code_new_item[0].item_code,
        "qty_dummy": float(promo_prod[0].total_quantity),
        "qty": float(promo_prod[0].total_quantity),
        "uom": "Case",
    })
    try:
        material_receipt_doc.insert(ignore_permissions=True)
        frappe.db.sql(""" UPDATE `tabPromo Production` SET stock_entry_name=%s  WHERE name=%s""",
                      (material_receipt_doc.name, promo_prod[0].name))
        material_receipt_doc.submit()
    except Exception:
        print(frappe.get_traceback())


def check_promo_sales():
    branch = frappe.db.get_value("Server Information", None, "branch")
    email_text = ""
    import datetime
    year_now = datetime.date.today().year
    date_start = datetime.datetime(year_now,1,1).date()
    to_date = datetime.datetime.now().date()
    items = frappe.db.sql("""SELECT itm.name, itm.barcode_retial from `tabItem` itm join `tabItem Branch` branch on itm.name = branch.parent
                            where itm.type = 'Company Bundling' and branch.branch = %s""",branch)
    for item in items:
        #print item[0]
        promo_allocation = float(get_promotions_allocation(item[0]))
        item_sales = float(get_item_sales(branch,date_start, to_date, item[1]))

        if promo_allocation > 0:
            print item[0], promo_allocation, item_sales
            if item_sales > promo_allocation:
                msg_ = "Item "+ item[0] + "("+item[1]+") Sales ("+str(item_sales)+") Exceeded Allocation ("+str(promo_allocation)+").\n"
                email_text+= msg_
    if email_text!= "":
        send_email(attachments=[{"fname": "promo_items_with_excess_sales", "fcontent": email_text},], message = email_text)

def get_promotions_allocation(item_code):
    promo_alloc = frappe.db.sql("""SELECT total from `tabPromotions` where new_item_code = %s or existing_bundle_item = %s 
                                  order by date desc limit 1""",(item_code,item_code))
    if len(promo_alloc)>0:
        return promo_alloc[0][0] if promo_alloc[0][0] is not None else 0
    else:
        return 0

def get_item_sales(branch, from_date, to_date, barcode):
    sales_qty = frappe.db.sql("""SELECT SUM(qty) from `tabUpload POS` where branch = %s and trans_date >= %s and trans_date <=%s 
                    and barcode = %s""",(branch, from_date, to_date, barcode))

    if len(sales_qty) > 0:
        return sales_qty[0][0] if sales_qty[0][0] is not None else 0
    else:
        return 0




def send_email(attachments, message):
    branch = frappe.db.get_value("Server Information", None, "branch")
    content_message = "BRANCH: " + branch + "\n The following items have exceeded their sales.\n"
    content_message +=message
    frappe.sendmail(
        recipients=(["hvillanueva.gaisano@gmail.com","cdogaisano@yahoo.com"]),
        expose_recipients="header",
        sender="Administrator",
        subject="Items not Syncing in Branch",
        content=content_message,
        attachments=attachments,
        delayed=False
    )