# -*- coding: utf-8 -*-
# Copyright (c) 2018, bai and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, json, decimal
from frappe.model.document import Document

class PromoProduction(Document):
    def on_cancel(self):
        from gaisano.seevents import update_stock_ledger
        if self.stock_entry_name:
            se_doc = frappe.get_doc("Stock Entry", self.stock_entry_name)
            se_doc.docstatus = 2
            update_stock_ledger(se_doc)
            frappe.db.sql(""" DELETE FROM `tabStock Entry` WHERE name = %s""", self.stock_entry_name)
            frappe.db.sql(""" DELETE FROM `tabGL Entry` WHERE voucher_no = %s""", self.stock_entry_name)
            promotions_data = frappe.db.sql(""" SELECT * FROM `tabPromotions` WHERE name=%s """,(self.promo_name),as_dict=True)
            promotions_budget_data = frappe.db.sql(""" SELECT * FROM `tabPromotions Budget` WHERE name=%s """,(self.debit_memo),as_dict=True)
            frappe.db.sql(""" UPDATE `tabPromotions` SET allocation_budget=%s WHERE name=%s """,(str(int(self.total_quantity) + int(promotions_data[0].allocation_budget)),self.promo_name))
            frappe.db.sql(""" UPDATE `tabPromotions Budget` SET budget_balance=%s WHERE name=%s """,(str(float(self.total_costs) + float(promotions_budget_data[0].budget_balance)),self.debit_memo))
    def on_submit(self):
        data = ""
        str = []
        self.create_material_issue()
        bal = frappe.db.sql(""" SELECT budget_balance FROM `tabPromotions Budget` WHERE name=%s """,self.debit_memo, as_dict=True)

        if (float(bal[0].budget_balance) > 0):
            if float(bal[0].budget_balance) >= float(self.total_costs):
                total_bb = float(bal[0].budget_balance) - float(self.total_costs)
                frappe.db.sql(""" UPDATE `tabPromotions Budget` SET budget_balance=%s WHERE name=%s""",
                              (total_bb, self.debit_memo))
                frappe.db.sql(""" UPDATE `tabPromotions` SET total_budget=%s WHERE name=%s""",
                              (total_bb, self.promo_name))
            else:
                frappe.throw("Budget Balance for Promotion is Insufficient. Production denied")
        else:
            frappe.throw("Invalid Promotions Budget")
        #update allocation budget
        allocation_budget = frappe.db.sql(""" SELECT allocation_budget FROM `tabPromotions` WHERE name=%s""",
                                          (self.promo_name), as_dict=True)

        if len(allocation_budget) > 0:

            if float(allocation_budget[0].allocation_budget) >= float(int(self.total_quantity)):

                total = float(allocation_budget[0].allocation_budget) - float(self.total_quantity)
                # print total
                frappe.db.sql(""" UPDATE `tabPromotions` SET allocation_budget=%s WHERE name=%s""",
                              (total, self.promo_name))
            else:
                frappe.throw("Allocated budget is insufficient for Production.")

    def create_material_issue(self):

        warehouse_default_promo = frappe.db.sql(""" SELECT name FROM `tabWarehouse` WHERE name LIKE %s """, ("Promo%"),
                                                as_dict=True)

        item_name_in_promotions = frappe.db.sql(""" SELECT * FROM `tabPromotions` WHERE name=%s """, (self.promo_name),as_dict=True)
        promo_budget_branch_code = frappe.db.sql(""" SELECT branch_code FROM `tabPromotions Budget` WHERE name=%s """,(self.debit_memo),as_dict=True)

        get_uom = item_name_in_promotions[0].promo_uom
        ##get_uom = frappe.db.sql(""" SELECT * FROM `tabUOM Conversion Detail`
        #                           WHERE parent=%s """,
        #                        item_name_in_promotions[0].existing_bundle_item if item_name_in_promotions[0].existing_bundle_item else item_name_in_promotions[0].new_item_code,as_dict=True)
        material_receipt_doc = frappe.get_doc({
            "doctype": "Stock Entry",
            "type": "Stock Transfer",
            "branch": promo_budget_branch_code[0].branch_code,
            "naming_series": "PROMO-." + self.branch_code + ".-.YY.MM.DD",
        })
        material_receipt_doc.append("items", {
            "s_warehouse": warehouse_default_promo[0].name,
            "t_warehouse": self.target_warehouse,
            "item_code": item_name_in_promotions[0].existing_bundle_item if item_name_in_promotions[0].existing_bundle_item else item_name_in_promotions[0].new_item_code,
            "qty_dummy": float(self.total_quantity),
            "qty": float(self.total_quantity),
            "uom": get_uom,
            "stock_uom": get_uom,
            "conversion_factor": 1, #getconversion factor
            "transfer_qty": float(self.total_quantity),
            "actual_qty": float(self.total_quantity)
        })

        try:
            material_receipt_doc.insert(ignore_permissions=True)
            frappe.db.sql(""" UPDATE `tabPromo Production` SET stock_entry_name=%s  WHERE name=%s""",(material_receipt_doc.name,self.name))
            material_receipt_doc.submit()
            self.reload()
        except Exception:
            frappe.log_error(frappe.get_traceback())

@frappe.whitelist()
def recreate_stock_entry(name):
    doc = frappe.get_doc("Promo Production", name)
    doc.create_material_issue()

    return True
@frappe.whitelist()
def compute_total_costs(total_quantity, promo_name,purchase_order,receiving_report ,purchase_invoice,cellophane_subtotal,scotchtape_subtotal,sealer_subtotal, production_costs_table, edit_srp):
    productions_costs = json.loads(production_costs_table.decode('utf-8')) if (production_costs_table).decode('utf-8') else {}
    total_costs = 0
    srp_value = 0
    subtotal_op = 0
    if total_quantity > 0 and (edit_srp == "false" or not edit_srp or edit_srp == "0"):

        free_item = 0
        free_item_initial = 0
        rebate_value = 0

        promotions_data = frappe.db.sql(""" SELECT * FROM `tabPromotions` WHERE name=%s """, (promo_name),
                                        as_dict=True)
        if len(promotions_data) > 0:
            promo_item = frappe.db.sql("""SELECT * FROM `tabPromo Items` WHERE parent=%s""", promo_name, as_dict=True)
            if promotions_data[0].promotion_scheme == "Buy x Take x" or promotions_data[0].promotion_scheme == "Ka Negosyo":
                for i in promo_item:
                    if i.free_item:
                        if promotions_data[0].uom == 'Case':
                            price_rate = frappe.db.sql(""" SELECT item_price FROM `tabItem` WHERE item_code=%s """,(i.promo_item), as_dict=True)
                            string_value = (round((float(price_rate[0].item_price) / float(promotions_data[0].pcs_in_pack)), 2))
                        else:
                            price_rate = frappe.db.sql(""" SELECT item_price_retail_with_margin FROM `tabItem` WHERE item_code=%s """,(i.promo_item),as_dict=True)
                            string_value = (round((float(price_rate[0].item_price_retail_with_margin) / float(promotions_data[0].pcs_in_pack)), 2))
                        string_value = float(get_rounded_price(string_value))
                        # Code ni Jiloy part X
                        # if str(string_value[len(string_value) - 2]) == ".":
                        #     string_value += "0"
                        # if int(string_value[len(string_value) - 1]) > 5:
                        #
                        #     if int(string_value[len(string_value) - 2]) == 9:
                        #         string_value = round(float(string_value))
                        #     else:
                        #         new_value = list(string_value)
                        #         new_value[len(string_value) - 1] = "0"
                        #         new_value[len(string_value) - 2] = str(int(string_value[len(string_value) - 2]) + 1)
                        #         string_value = "".join(new_value)
                        # elif int(string_value[len(string_value) - 1]) < 5 and int(
                        #         string_value[len(string_value) - 1]) > 0:
                        #     new_value = list(string_value)
                        #     new_value[len(string_value) - 1] = "5"
                        #     string_value = "".join(new_value)
                        free_item += float(string_value) * float(i.quantity_pitem)

                srp_value += (float(free_item) * float(total_quantity))
            elif promotions_data[0].promotion_scheme == "Rebate":
                if promotions_data[0].percentage:
                    rebate_value_over_percentage = float(promotions_data[0].rebate) / float(100)
                    price_rate_value = float(promo_item[0].price_rate)
                    rebate_discount = (float(price_rate_value * rebate_value_over_percentage))
                    rounded_rebate = decimal.Decimal(str(rebate_discount)).quantize(decimal.Decimal('.01'),
                                                                     rounding=decimal.ROUND_HALF_UP)
                    srp_value_temp = float(rounded_rebate)*float(total_quantity)
                    srp_value = float(get_rounded_price(srp_value_temp))
                    # test_string = "rebate % " + str(rebate_value_over_percentage) + "| price_rate_value: " + \
                    #               str(price_rate_value) + "| rounded rebate: " + str(rounded_rebate) + "srp: " + str(
                    #     srp_value_temp) + "| Rounded: " + str(srp_value)
                    # frappe.msgprint(test_string)
                    # CODE NI JILOY
                        # if price_rate_times_rebate[len(price_rate_times_rebate) - 4] == ".":
                        #     value_int = int(price_rate_times_rebate[len(price_rate_times_rebate)-2])
                        #     value_int_ = int(price_rate_times_rebate[len(price_rate_times_rebate)-3])
                        #     if int(price_rate_times_rebate[len(price_rate_times_rebate)-1]) >= 5 and int(price_rate_times_rebate[len(price_rate_times_rebate)-1]) < 9:
                        #         value_int += 1
                        #         final_price_rate = str(int(float(price_rate_value * rebate_value_over_percentage))) + "." +  price_rate_times_rebate[len(price_rate_times_rebate) - 3] + str(value_int)
                        #     elif int(price_rate_times_rebate[len(price_rate_times_rebate)-1]) == 9:
                        #         value_int_ += 1
                        #         final_price_rate = str(int(float(price_rate_value * rebate_value_over_percentage))) + "." +  str(value_int_) + "0"
                        # else:
                        #     final_price_rate = str(round(float(price_rate_value * rebate_value_over_percentage),2))
                        #
                        # first_amount = final_price_rate
                        # if str(first_amount[len(first_amount) - 2]) == ".":
                        #     first_amount += "0"
                        # if int(first_amount[len(first_amount) - 1]) > 5:
                        #
                        #     if int(first_amount[len(first_amount) - 2]) == 9:
                        #         first_amount = round(float(first_amount))
                        #     else:
                        #         new_value = list(first_amount)
                        #         new_value[len(first_amount) - 1] = "0"
                        #         new_value[len(first_amount) - 2] = str(int(first_amount[len(first_amount) - 2]) + 1)
                        #         first_amount = "".join(new_value)
                        # elif int(first_amount[len(first_amount) - 1]) < 5 and int(
                        #         first_amount[len(first_amount) - 1]) > 0:
                        #     new_value = list(first_amount)
                        #     new_value[len(first_amount) - 1] = "5"
                        #     first_amount = "".join(new_value)
                        #
                        # string_value = str(round(float(first_amount) * float(total_quantity),2))
                        #
                        # if str(string_value[len(string_value) - 2]) == ".":
                        #     string_value += "0"
                        #
                        # if int(string_value[len(string_value) - 1]) > 5:
                        #
                        #     if int(string_value[len(string_value) - 2]) == 9:
                        #         string_value = round(float(string_value))
                        #     else:
                        #         new_value = list(string_value)
                        #         new_value[len(string_value) - 1] = "0"
                        #         new_value[len(string_value) - 2] = str(int(string_value[len(string_value) - 2]) + 1)
                        #         string_value = "".join(new_value)
                        # elif int(string_value[len(string_value) - 1]) < 5 and int(
                        #         string_value[len(string_value) - 1]) > 0:
                        #     new_value = list(string_value)
                        #     new_value[len(string_value) - 1] = "5"
                        #     string_value = "".join(new_value)
                        # srp_value = float(string_value)
                else:
                    srp_value = float(promotions_data[0].rebate) * float(total_quantity)

            elif promotions_data[0].promotion_scheme == "Suki Points":
                srp_value = float(promotions_data[0].suki_points) * float(total_quantity)

            elif promotions_data[0].promotion_scheme == "Buy x Take x + Rebate" or promotions_data[0].promotion_scheme == "Ka Negosyo + Rebate":
                for i in promo_item:
                    if i.free_item:
                        if promotions_data[0].uom == 'Case':
                            price_rate = frappe.db.sql(""" SELECT item_price FROM `tabItem` WHERE item_code=%s """,(i.promo_item), as_dict=True)
                            string_value = (round((float(price_rate[0].item_price) / float(promotions_data[0].pcs_in_pack)), 2))
                        else:
                            price_rate = frappe.db.sql(""" SELECT item_price_retail_with_margin FROM `tabItem` WHERE item_code=%s """,(i.promo_item),as_dict=True)
                            string_value = (round((float(price_rate[0].item_price_retail_with_margin) / float(promotions_data[0].pcs_in_pack)), 2))
                        string_value = get_rounded_price(string_value)
                        # CODE NI JILOY PART X
                        # if str(string_value[len(string_value) - 2]) == ".":
                        #     string_value += "0"
                        # if int(string_value[len(string_value) - 1]) > 5:
                        #
                        #     if int(string_value[len(string_value) - 2]) == 9:
                        #         string_value = round(float(string_value))
                        #     else:
                        #         new_value = list(string_value)
                        #         new_value[len(string_value) - 1] = "0"
                        #         new_value[len(string_value) - 2] = str(int(string_value[len(string_value) - 2]) + 1)
                        #         string_value = "".join(new_value)
                        # elif int(string_value[len(string_value) - 1]) < 5 and int(
                        #         string_value[len(string_value) - 1]) > 0:
                        #     new_value = list(string_value)
                        #     new_value[len(string_value) - 1] = "5"
                        #     string_value = "".join(new_value)
                        free_item += float(string_value) * float(i.quantity_pitem)

                free_item_initial += (float(free_item) * float(total_quantity))
                if promotions_data[0].percentage:
                    first_amount = str(round(float(promo_item[0].price_rate) * (float(promotions_data[0].rebate) / float(100)),2))
                    if str(first_amount[len(first_amount) - 2]) == ".":
                        first_amount += "0"
                    if int(first_amount[len(first_amount) - 1]) > 5:

                        if int(first_amount[len(first_amount) - 2]) == 9:
                            first_amount = round(float(first_amount))
                        else:
                            new_value = list(first_amount)
                            new_value[len(first_amount) - 1] = "0"
                            new_value[len(first_amount) - 2] = str(int(first_amount[len(first_amount) - 2]) + 1)
                            first_amount = "".join(new_value)
                    elif int(first_amount[len(first_amount) - 1]) < 5 and int(
                            first_amount[len(first_amount) - 1]) > 0:
                        new_value = list(first_amount)
                        new_value[len(first_amount) - 1] = "5"
                        first_amount = "".join(new_value)

                    string_value = str(round(float(first_amount) * float(total_quantity),2))

                    if str(string_value[len(string_value) - 2]) == ".":
                        string_value += "0"
                    if int(string_value[len(string_value) - 1]) > 5:

                        if int(string_value[len(string_value) - 2]) == 9:
                            string_value = round(float(string_value))
                        else:
                            new_value = list(string_value)
                            new_value[len(string_value) - 1] = "0"
                            new_value[len(string_value) - 2] = str(int(string_value[len(string_value) - 2]) + 1)
                            string_value = "".join(new_value)
                    elif int(string_value[len(string_value) - 1]) < 5 and int(
                            string_value[len(string_value) - 1]) > 0:
                        new_value = list(string_value)
                        new_value[len(string_value) - 1] = "5"
                        string_value = "".join(new_value)
                    rebate_value = float(string_value)
                else:
                    rebate_value = float(promotions_data[0].rebate) * float(total_quantity)
                srp_value = float(free_item_initial) + float(rebate_value)
            if purchase_order or receiving_report or purchase_invoice or promotions_data[0].free_goods_delivered_by_supplier:
                srp_value = 0


        for x in range(0,len(productions_costs)):
            if productions_costs[x]['operating_cost']:
                total_costs += float(productions_costs[x]['operating_cost']) * float(total_quantity)
        subtotal_op = total_costs
        total_costs = total_costs + float(cellophane_subtotal) + float(scotchtape_subtotal) + float(sealer_subtotal)
        total_costs += float(srp_value)

        return round(total_costs, 2), srp_value, subtotal_op
    elif total_quantity > 0 and (edit_srp or edit_srp == "1"):

        for x in range(0,len(productions_costs)):
            if productions_costs[x]['operating_cost']:
                total_costs += float(productions_costs[x]['operating_cost']) * float(total_quantity)
        subtotal_op = total_costs
        total_costs = total_costs + float(cellophane_subtotal) + float(scotchtape_subtotal) + float(sealer_subtotal)
        total_costs += float(srp_value)
        total_costs = round(total_costs,2)
        return round(total_costs, 2), subtotal_op
# @frappe.whitelist()
# def refresh_total_costs(total_costs, name):
#     frappe.db.sql(""" UPDATE `tabPromo Production` SET total_costs=%s WHERE name=%s """,(total_costs,name))
#     frappe.db.commit()
@frappe.whitelist()
def get_required_items(promo_series):
    print(promo_series)
    item_name_array = []
    data = frappe.db.sql(""" SELECT * FROM `tabPromo Items` WHERE parent=%s """,(promo_series), as_dict=True)
    data_other_item = frappe.db.sql(""" SELECT * FROM `tabPromotions Other Items` WHERE parent=%s """,(promo_series), as_dict=True)
    for i in data:
        item_name_array.append(i.description)
    for ii in data_other_item:
        item_name_array.append(ii.other_name)
    allocation_balance = frappe.db.sql(""" SELECT * FROM `tabPromotions` WHERE name=%s """,(promo_series), as_dict=True)
    warehouse_default = frappe.db.sql(""" SELECT name FROM `tabWarehouse` WHERE name LIKE %s """, ("Promo%"),
                                      as_dict=True)
    production_costs = frappe.db.sql(""" SELECT * FROM `tabProduction Costs` """,as_dict=True)
    return data,allocation_balance,warehouse_default,production_costs,item_name_array,data_other_item

@frappe.whitelist()
def get_all():
    data = frappe.db.sql(""" SELECT * FROM `tabProduction Costs` """, as_dict=True)
    return data

@frappe.whitelist()
def get_cellophane_price(cellophane):
    data = frappe.db.sql(""" SELECT price FROM `tabCellophane` WHERE size=%s """,(cellophane),as_dict=True)
    return data

@frappe.whitelist()
def get_scotch_tape_price(scotch_tape):
    data = frappe.db.sql(""" SELECT price FROM `tabScotch Tape` WHERE size=%s """,(scotch_tape),as_dict=True)
    return data

@frappe.whitelist()
def raise_error(total_quantity):
    frappe.throw("The maximum allowed qty is " + total_quantity)

@frappe.whitelist()
def get_promotion_scheme(promo_name):
    promotion_data = frappe.db.sql(""" SELECT * FROM `tabPromotions` WHERE name=%s """,(promo_name), as_dict=True)
    return promotion_data

@frappe.whitelist()
def get_warehouse():
    return frappe.db.sql(""" select name from tabWarehouse where name like %s """,("%CDO Warehouse%"), as_dict=True)
@frappe.whitelist()
def update_promo_prod(name,total_costs,srp,suptotal_op):
    frappe.db.sql(""" UPDATE `tabPromo Production` set total_costs=%s, srp=%s, subtotal_op=%s WHERE name=%s """,(total_costs,srp,suptotal_op,name), as_dict=True)
    frappe.db.commit()

@frappe.whitelist()
def test():
    print(frappe.get_doc("Stock Entry", ""))


# childquery = """UPDATE {0} set docstatus = 2 where parent = '{1}'""".format(child_table, r_id)
# parentquery = """UPDATE {0} set docstatus = 2, workflow_state = 'Cancel' where name = '{1}'""".format(table_name, r_id)
# frappe.db.sql(childquery)
# frappe.db.sql(parentquery)
# if count_sle[0][0] > 0:
#     print "SE.... attempting to cancel and adjust sle's"
#     from gaisano.seevents import update_stock_ledger
#
#     se_doc = frappe.get_doc("Stock Entry", r_id)
#     se_doc.docstatus = 2
#     update_stock_ledger(se_doc)

def get_rounded_price(price):
	final= 0
	price = float(price)
	rounded = round(price, 1)
	truncated = round(price,2)
	diff = round(float(rounded - truncated),2)
	print "R1:" , rounded, "|R2:", truncated, "|DIFF=", diff
	if ((diff == 0) or (diff == 0.05) or (diff == -0.05)):
		final = truncated
	elif ((diff >= 0.01) and (diff <= .04)):
		final = rounded

	else:
		final = (rounded + 0.05)
	return final