

cur_frm.cscript.edit_subtotal = function (frm,cdt,cdn) {
    cur_frm.set_df_property('promotions_subtotal', 'read_only', !cur_frm.doc.edit_subtotal);
    cur_frm.refresh_field("promotions_subtotal");
}
// Copyright (c) 2019, bai and contributors
// For license information, please see license.txt
// cur_frm.cscript.other_selling = function (frm,cdt,cdn) {
// 	var df = locals[cdt][cdn]
// 	if(df.other_selling){
// 		df.other_qty = parseInt(df.other_warehouse,10) * parseInt(df.other_packing,10) + parseInt(df.other_selling)
// 		if(cur_frm.doc.promotion_scheme === "Rebate"){
// 			df.other_sub_total = parseFloat(df.other_qty) * parseFloat(df.other_rebates)
// 		} else if(cur_frm.doc.promotion_scheme === "Suki Points"){
// 			df.other_sub_total = parseFloat(df.other_qty) * parseFloat(df.other_points)
// 		} else {
// 			df.other_sub_total = parseFloat(df.other_qty) * parseFloat(df.other_srp)
// 		}
// 		cur_frm.refresh_field("other_items_table")
//
// 		var total_qty = compute_promotions_total(frm)
// 		compute_operations_total(frm,total_qty)
// 	}
// }
// cur_frm.cscript.other_warehouse = function (frm,cdt,cdn) {
// 	var df = locals[cdt][cdn]
// 	if(df.other_warehouse){
// 		df.other_qty = parseInt(df.other_warehouse,10) * parseInt(df.other_packing,10) + parseInt(df.other_selling)
// 		if(cur_frm.doc.promotion_scheme === "Rebate"){
// 			df.other_sub_total = parseFloat(df.other_qty) * parseFloat(df.other_rebates)
// 		} else if(cur_frm.doc.promotion_scheme === "Suki Points"){
// 			df.other_sub_total = parseFloat(df.other_qty) * parseFloat(df.other_points)
// 		} else {
// 			df.other_sub_total = parseFloat(df.other_qty) * parseFloat(df.other_srp)
// 		}
// 		cur_frm.refresh_field("other_items_table")
//
// 		var total_qty = compute_promotions_total(frm)
// 		compute_operations_total(frm,total_qty)
// 	}
// }
// cur_frm.cscript.other_points = function (frm,cdt,cdn) {
// 	var df = locals[cdt][cdn]
//
// 	df.other_sub_total = parseFloat(df.other_points) * parseFloat(df.other_qty)
//
// 	var total_qty = compute_promotions_total(frm)
// 	compute_operations_total(frm,total_qty)
// }
// cur_frm.cscript.other_packing_per_piece = function (frm,cdt,cdn) {
// 	var df = locals[cdt][cdn]
// 	var reb_val = (Math.round(100 * (parseFloat(df.other_srp) / parseFloat(df.packing_per_piece))) /100 ).toFixed(2).toString()
//
//
// 	if(parseInt(reb_val[reb_val.length - 1]) > 5){
// 		if(parseInt(reb_val[reb_val.length - 2]) !== 9){
// 			var increment_number = parseInt(reb_val[reb_val.length - 2]) + 1
// 			var changed_reb_value = reb_val.slice(0, reb_val.length-2) + increment_number.toString() + "0"
// 			reb_val = parseFloat(changed_reb_value)
// 		} else {
// 			reb_val = Math.ceil(parseFloat(reb_val))
// 		}
// 	} else if (parseInt(reb_val[reb_val.length - 1]) < 5 && parseInt(reb_val[reb_val.length - 1]) > 0) {
//
// 		reb_val = parseFloat(reb_val.substring(0, reb_val.length - 1) + "5")
// 	}
//
// 		df.other_srp = reb_val
// 		df.other_sub_total = parseFloat(df.other_srp) * parseFloat(df.other_qty)
// 	cur_frm.refresh_field("other_items_table")
// 	var total_qty = compute_promotions_total(frm)
// 	compute_operations_total(frm,total_qty)
// }
// cur_frm.cscript.other_qty = function (frm,cdt,cdn) {
// 	var df = locals[cdt][cdn]
// 	if(df.other_qty) {
//
// 		if(frm.promotion_scheme === "Buy x Take x" || frm.promotion_scheme === "Ka Negosyo"){
// 			df.other_sub_total = parseFloat(parseFloat(df.other_srp) * (parseFloat(df.other_qty)))
// 			cur_frm.refresh_field("other_items_table")
// 		}
// 		else if(frm.promotion_scheme === "Rebate"){
// 			df.other_sub_total = parseFloat(df.other_rebates) * parseFloat(df.other_qty)
// 			cur_frm.refresh_field("other_items_table")
// 		}
// 		else if(cur_frm.doc.promotion_scheme === "Buy x Take x + Rebate") {
// 			df.other_sub_total = (parseFloat(df.other_rebates) * parseFloat(df.other_qty)) + (parseFloat(df.other_srp) * parseFloat(df.other_qty))
// 			cur_frm.refresh_field("other_items_table")
// 		}
// 		else if(cur_frm.doc.promotion_scheme === "Suki Points") {
// 			df.other_sub_total = parseFloat(df.other_points) * parseFloat(df.other_qty)
// 			cur_frm.refresh_field("other_items_table")
// 		}
//
// 		var total_qty = compute_promotions_total(frm)
// 		compute_operations_total(frm,total_qty)
// 	}
//
// }
//
// cur_frm.cscript.other_name = function (frm,cdt,cdn) {
// 	var df = locals[cdt][cdn]
// 	if(df.other_name) {
//         frappe.call({
//             method: "gaisano_promotions.gaisano_promotions.doctype.promotions_calculation.promotions_calculation.get_other_item",
//             args: {
//                 promo_item: df
//             },
//             callback: function (r) {
//             	console.log(r.message)
//                 df.other_srp = r.message[0][0].srp
//                 df.other_qty = 1
// 				df.other_item_cost = r.message[0][0].item_cost
//                 df.other_margin = r.message[0][0].margin
// 				df.other_packing = r.message[0][0].packing
// 				if(frm.promotion_scheme === "Buy x Take x" || frm.promotion_scheme === "Ka Negosyo"){
// 					df.other_sub_total = parseFloat(parseFloat(df.other_srp) * (parseFloat(df.other_qty)))
// 					cur_frm.refresh_field("other_items_table")
// 				}
// 				else if(frm.promotion_scheme === "Rebate"){
// 					df.other_sub_total = parseFloat(df.other_rebates) * parseFloat(df.other_qty)
// 					cur_frm.refresh_field("other_items_table")
// 				}
// 				else if(cur_frm.doc.promotion_scheme === "Buy x Take x + Rebate") {
// 					df.other_sub_total = (parseFloat(df.other_rebates) * parseFloat(df.other_qty)) + (parseFloat(df.other_srp) * parseFloat(df.other_qty))
// 					cur_frm.refresh_field("other_items_table")
// 				}
// 				else if(cur_frm.doc.promotion_scheme === "Suki Points") {
// 					df.other_sub_total = parseFloat(df.points) * parseFloat(df.qty)
// 					cur_frm.refresh_field("other_items_table")
// 				}
// 		var total_qty = compute_promotions_total(frm)
// 		compute_operations_total(frm,total_qty)
//             }
//         })
//     }
// }

cur_frm.cscript.edit_subtotal = function (frm,cdt,cdn) {
	cur_frm.set_df_property('promotions_subtotal', 'read_only', !cur_frm.doc.edit_subtotal);
		cur_frm.refresh_field("promotions_subtotal");
}

cur_frm.cscript.promo_item = function (frm,cdt,cdn) {
	var df = locals[cdt][cdn]

	frappe.call({
		method: "gaisano_promotions.gaisano_promotions.doctype.promotions_calculation.promotions_calculation.get_promo_item",
		args: {
			promo_item: df,
			supplier: cur_frm.doc.supplier ? cur_frm.doc.supplier : 0
		},
		callback: function (r) {
			if(frm.promotion_scheme === "Buy x Take x" || frm.promotion_scheme === "Buy x Take x + Rebate" || frm.promotion_scheme === "Suki Points"){
				df.srp = r.message[0].item_price_retail_with_margin
				df.description = r.message[0].item_name_dummy
				df.qty = 1
				df.sub_total = parseFloat(r.message[0].item_price_retail_with_margin * (parseFloat(df.qty)))
				cur_frm.refresh_field("promotions")
			} else if(frm.promotion_scheme === "Ka Negosyo" || cur_frm.doc.promotion_scheme === "Ka Negosyo + Rebate"){
				df.srp = r.message[0].item_price
				df.description = r.message[0].item_name_dummy
				df.qty = 1
				df.sub_total = parseFloat(r.message[0].item_price * (parseFloat(df.qty)))
				cur_frm.refresh_field("promotions")
			} else if(frm.promotion_scheme === "Rebate"){
				df.srp = r.message[0].item_price_retail_with_margin
				df.description = r.message[0].item_name_dummy
				df.qty = 1
				compute_rebates_items(cur_frm)
			}
			var total_qty = compute_promotions_total(frm)
			compute_operations_total(frm,total_qty)
        }
	})
}
cur_frm.cscript.rebate_value = function (frm,cdt,cdn) {
	compute_rebates_items(cur_frm)
	//compute_rebates_other_items(cur_frm)
	var total_qty = compute_promotions_total(frm)
	compute_operations_total(frm,total_qty)


}
cur_frm.cscript.rebates = function (frm,cdt,cdn) {
	var d = locals[cdt][cdn]
	var total_costs = 0
	if(cur_frm.doc.promotion_scheme === "Rebate"){
		d.sub_total = parseFloat(d.rebates) * parseFloat(d.qty)
	} else 	if(cur_frm.doc.promotion_scheme === "Buy x Take x + Rebate") {
		d.sub_total = (parseFloat(d.rebates) * parseFloat(d.qty)) + (parseFloat(d.srp) * parseFloat(d.qty))
	} else 	if(cur_frm.doc.promotion_scheme === "Ka Negosyo + Rebate") {
		d.sub_total = (parseFloat(d.rebates) * parseFloat(d.warehouse))
	}
	cur_frm.refresh_field("promotions")
	var total_qty = compute_promotions_total(frm)
	compute_operations_total(frm,total_qty)
}
cur_frm.cscript.percentage = function (frm,cdt,cdn) {
	cur_frm.set_df_property('rebate_value', 'hidden', !cur_frm.doc.percentage);
	cur_frm.refresh_field("rebate_value")
	var df = frappe.meta.get_docfield("Promotions Calculation Table", "rebates", cur_frm.doc.name);
	df.read_only = cur_frm.doc.percentage
	cur_frm.refresh_field("promotions")
}

cur_frm.cscript.qty = function (frm,cdt,cdn) {
	var df = locals[cdt][cdn]

	if(frm.promotion_scheme === "Buy x Take x" || frm.promotion_scheme === "Ka Negosyo"){
		df.sub_total = parseFloat(parseFloat(df.srp) * (parseFloat(df.qty)))
		cur_frm.refresh_field("promotions")
	} else if(frm.promotion_scheme === "Rebate"){
		df.sub_total = parseFloat(df.rebates) * parseFloat(df.qty)
		cur_frm.refresh_field("promotions")
	} else 	if(cur_frm.doc.promotion_scheme === "Buy x Take x + Rebate" || cur_frm.doc.promotion_scheme === "Ka Negosyo + Rebate") {
		df.sub_total = (parseFloat(df.rebates) * parseFloat(df.qty)) + (parseFloat(df.srp) * parseFloat(df.qty))
				cur_frm.refresh_field("promotions")
	} else 	if(cur_frm.doc.promotion_scheme === "Suki Points") {
		df.sub_total = parseFloat(df.points) * parseFloat(df.qty)
		cur_frm.refresh_field("promotions")
	}

	var total_qty = compute_promotions_total(frm)
	compute_operations_total(frm,total_qty)
}

cur_frm.cscript.promotion_scheme = function (frm,cdt,cdn) {
	var df = frappe.meta.get_docfield("Promotions Calculation Table", "rebates", cur_frm.doc.name);
	var df1 = frappe.meta.get_docfield("Promotions Calculation Table", "free_item_qty", cur_frm.doc.name);
	var df2 = frappe.meta.get_docfield("Promotions Calculation Table", "points", cur_frm.doc.name);
	var df3 = frappe.meta.get_docfield("Promotions Calculation Table", "regular_item", cur_frm.doc.name);

	if(cur_frm.doc.promotion_scheme === "Buy x Take x"){
		df.read_only = 1
		df1.hidden = 0
		df2.hidden = 1
		df3.hidden = 0
		cur_frm.set_df_property('percentage', 'hidden', 1);
		cur_frm.refresh_field("percentage")
		cur_frm.set_df_property('rebate_value', 'hidden', 1);
		cur_frm.refresh_field("rebate_value")
	} else if(cur_frm.doc.promotion_scheme === "Rebate"){
		df.read_only = 0
		df1.hidden = 1
		df2.hidden = 1
		df3.hidden = 1
		cur_frm.set_df_property('percentage', 'hidden', 0);
		cur_frm.refresh_field("percentage")

	} else if(cur_frm.doc.promotion_scheme === "Ka Negosyo"){
		df1.hidden = 0
		df2.hidden = 1
		df.read_only = 0
		df3.hidden = 0

		cur_frm.set_df_property('percentage', 'hidden', 1);
		cur_frm.refresh_field("percentage")
		cur_frm.set_df_property('rebate_value', 'hidden', 1);
		cur_frm.refresh_field("rebate_value")
	} else if(cur_frm.doc.promotion_scheme === "Buy x Take x + Rebate"){
		df1.hidden = 0
		df2.hidden = 1
		df.read_only = 0
		df3.hidden = 0

		cur_frm.set_df_property('percentage', 'hidden', 0);
		cur_frm.refresh_field("percentage")
	} else if(cur_frm.doc.promotion_scheme === "Suki Points"){
		df1.hidden = 1
		df2.hidden = 0
		df.read_only = 1
		df3.hidden = 1
		cur_frm.set_df_property('percentage', 'hidden', 1);
		cur_frm.refresh_field("percentage")
		cur_frm.set_df_property('rebate_value', 'hidden', 1);
		cur_frm.refresh_field("rebate_value")
	} else if(cur_frm.doc.promotion_scheme === "Ka Negosyo + Rebate"){
		df1.hidden = 0
		df2.hidden = 1
		df.read_only = 0
		df3.hidden = 0

		cur_frm.set_df_property('percentage', 'hidden', 0);
		cur_frm.refresh_field("percentage")
	}

	cur_frm.doc.promotions = undefined
	cur_frm.doc.promotions_subtotal = 0
	cur_frm.refresh_field("promotions")
	cur_frm.refresh_field("promotions_subtotal")
}
cur_frm.cscript.operation = function (frm,cdt,cdn) {
	var df = locals[cdt][cdn]
	console.log(df)
	frappe.call({
		method: "gaisano_promotions.gaisano_promotions.doctype.promotions_calculation.promotions_calculation.get_operation",
		args: {
			operation: df
		},
		callback: function (r) {
			df.operating_cost = parseFloat(r.message[0].price)
			var total_qty = 0.00
			for(var i=0;i<frm.promotions.length;i+=1){
				if(cur_frm.doc.promotion_scheme === "Buy x Take x"){
					total_qty += (parseFloat(frm.promotions[i].qty) / 2)
				} else {
					total_qty += parseFloat(frm.promotions[i].qty)
				}
			}
			df.sub_total = parseFloat(df.operating_cost) * parseFloat(total_qty)
			var operation_costs_total = 0.00
			for(var x=0;x<frm.operations.length;x+=1){
				operation_costs_total += parseFloat(frm.operations[x].sub_total)
			}
			cur_frm.doc.promo_costs_subtotal = parseFloat(operation_costs_total)
			cur_frm.refresh_field("promo_costs_subtotal")
			cur_frm.refresh_field("operations")
		}
    })
}
cur_frm.cscript.onload = function (frm) {
	if(cur_frm.doc.name.includes("New Promotions Calculation")){
		frappe.call({
			method: "gaisano_promotions.gaisano_promotions.doctype.promotions_calculation.promotions_calculation.get_operation_sealer",
			args: {},
			callback: function (r) {
				cur_frm.doc.sealer = r.message[0].price

				cur_frm.refresh_field("sealer")
			}
		})
    	var xx=0
         if(frm.operations !== undefined){
			var productionCosts = frm.operations.length
			while (xx < productionCosts) {
				cur_frm.get_field("operations").grid.grid_rows[0].remove()
				xx+=1;
			}
         }
		if(frm.operations === undefined) {
			 frappe.call({
				 method: "gaisano_promotions.gaisano_promotions.doctype.promo_production.promo_production.get_all",
				 args: {},
				 callback: function (r) {
				 	var operation_costs_total = 0
					for (var v = 0; v < r.message.length; v++) {
						if((r.message[v].operation).toLowerCase() !== "sealer"){
							var new_row = cur_frm.add_child("operations")
							new_row.operation = r.message[v].operation;
							new_row.operating_cost = r.message[v].price;
							new_row.sub_total = r.message[v].price;
						} else {
							cur_frm.set_value("sealer", r.message[v].price)
						}
					}
						refresh_field("sealer")
						refresh_field("operations")
					 for(var x=0;x<frm.operations.length;x+=1){
						operation_costs_total += parseFloat(frm.operations[x].sub_total)
					}
			cur_frm.doc.promo_costs_subtotal = parseFloat(operation_costs_total)
			cur_frm.refresh_field("promo_costs_subtotal")

				}
			 })

		}

}
	if(cur_frm.doc.promotion_scheme === "Rebate" || cur_frm.doc.promotion_scheme === "Buy x Take x + Rebate" || cur_frm.doc.promotion_scheme === "Ka Negosyo + Rebate"){
		cur_frm.set_df_property('percentage', 'hidden', 0);
		cur_frm.refresh_field("percentage")

		cur_frm.set_df_property('rebate_value', 'hidden', !cur_frm.doc.percentage);
		cur_frm.refresh_field("rebate_value")
	}
	if(cur_frm.doc.promotion_scheme === "Ka Negosyo + Rebate" || cur_frm.doc.promotion_scheme === "Ka Negosyo" || cur_frm.doc.promotion_scheme === "Buy x Take x + Rebate" || cur_frm.doc.promotion_scheme === "Buy x Take x"){
		var df = frappe.meta.get_docfield("Promotions Calculation Table", "free_item_qty", cur_frm.doc.name);
		var df1 = frappe.meta.get_docfield("Promotions Calculation Table", "regular_item", cur_frm.doc.name);
		df.hidden = 0
		df1.hidden = 0
	}
	if(cur_frm.doc.promotion_scheme === "Suki Points"){
		var df5 = frappe.meta.get_docfield("Promotions Calculation Table", "points", cur_frm.doc.name);
		df5.hidden = 0
	}

}
cur_frm.cscript.hours = function (frm) {
	if(cur_frm.doc.sealer){
		if(cur_frm.doc.hours){
			cur_frm.doc.sealer_subtotal = parseFloat(cur_frm.doc.sealer) * parseFloat(cur_frm.doc.hours)
			cur_frm.refresh_field("sealer_subtotal")
		} else {
			cur_frm.doc.sealer_subtotal = parseFloat(0)
			cur_frm.refresh_field("sealer_subtotal")
		}
	}
}
cur_frm.cscript.cellophane = function (frm,cdt,cdn) {
	if(cur_frm.doc.cellophane) {
        frappe.call({
            method: "gaisano_promotions.gaisano_promotions.doctype.promotions_calculation.promotions_calculation.get_cellophane_price",
            args: {
                cellophane: frm.cellophane
            },
            callback: function (r) {
                cur_frm.doc.cellophane_price = parseFloat(r.message[0].price)
                if (cur_frm.doc.cellophane_quantity) {
                    cur_frm.doc.cellophane_subtotal = parseFloat(cur_frm.doc.cellophane_price) * parseFloat(cur_frm.doc.cellophane_quantity)
                    cur_frm.refresh_field("cellophane_subtotal")
                }
                cur_frm.refresh_field("cellophane_price")
            }
        })
    } else {
		cur_frm.doc.cellophane_subtotal = parseFloat(0)
		cur_frm.doc.cellophane_price = parseFloat(0)
		cur_frm.doc.cellophane_quantity = ""
		cur_frm.refresh_field("cellophane_quantity")
		cur_frm.refresh_field("cellophane_price")
		cur_frm.refresh_field("cellophane_subtotal")
	}
}

cur_frm.cscript.scotch_tape = function (frm,cdt,cdn) {
	if(cur_frm.doc.scotch_tape){
		frappe.call({
		method: "gaisano_promotions.gaisano_promotions.doctype.promotions_calculation.promotions_calculation.get_scotchtape_price",
		args: {
			scotch_tape: frm.scotch_tape
		},
		callback: function (r) {
			cur_frm.doc.scotch_tape_price = parseFloat(r.message[0].price)
			if(cur_frm.doc.scotch_tape_quantity){
				cur_frm.doc.scotch_tape_subtotal = parseFloat(cur_frm.doc.scotch_tape_price) * parseFloat(cur_frm.doc.scotch_tape_quantity)
				cur_frm.refresh_field("scotch_tape_subtotal")
			}
			cur_frm.refresh_field("scotch_tape_price")
		}
	})
	} else {
		cur_frm.doc.scotch_tape_subtotal = parseFloat(0)
		cur_frm.doc.scotch_tape_price = parseFloat(0)
		cur_frm.doc.scotch_tape_quantity = ""
		cur_frm.refresh_field("scotch_tape_quantity")
		cur_frm.refresh_field("scotch_tape_price")
		cur_frm.refresh_field("scotch_tape_subtotal")
	}

}

cur_frm.cscript.cellophane_quantity = function (frm,cdt,cdn) {
	if(cur_frm.doc.cellophane){
		cur_frm.doc.cellophane_subtotal = parseFloat(cur_frm.doc.cellophane_price) * parseFloat(cur_frm.doc.cellophane_quantity)
		cur_frm.refresh_field("cellophane_subtotal")
	}
}

cur_frm.cscript.scotch_tape_quantity = function (frm,cdt,cdn) {
		if(cur_frm.doc.scotch_tape) {
            cur_frm.doc.scotch_tape_subtotal = parseFloat(cur_frm.doc.scotch_tape_price) * parseFloat(cur_frm.doc.scotch_tape_quantity)
            cur_frm.refresh_field("scotch_tape_subtotal")
        }
}
cur_frm.cscript.branch = function (frm) {
	if(cur_frm.doc.branch){
		frappe.call({
			method: "gaisano_promotions.gaisano_promotions.doctype.promotions_calculation.promotions_calculation.get_branch_code",
			args: {
				branch: cur_frm.doc.branch
			},
			callback: function (r) {
				cur_frm.doc.branch_code = r.message[0].branch_code
				cur_frm.refresh_field("branch_code")
			}
		})
	}
}

cur_frm.cscript.warehouse = function (frm,cdt,cdn) {
	var df = locals[cdt][cdn]
	if(df.warehouse){
		frappe.call({
			method: "gaisano_promotions.gaisano_promotions.doctype.promotions_calculation.promotions_calculation.get_promo_item",
			args: {
				promo_item: df,
				supplier: cur_frm.doc.supplier ? cur_frm.doc.supplier : 0
			},
			callback: function (r) {

				df.qty = cur_frm.doc.promotion_scheme === "Ka Negosyo" || cur_frm.doc.promotion_scheme === "Ka Negosyo + Rebate" ? parseInt(df.warehouse,10) : parseInt(df.warehouse,10) * parseInt(r.message[0].packing,10) + parseInt(df.selling)
				if(cur_frm.doc.promotion_scheme === "Rebate" || cur_frm.doc.promotion_scheme === "Ka Negosyo + Rebate"){
				    compute_rebates_items(cur_frm)
					df.sub_total = parseFloat(df.qty) * parseFloat(df.rebates)
				} else if(cur_frm.doc.promotion_scheme === "Suki Points"){
					df.sub_total = parseFloat(df.qty) * parseFloat(df.points)
				} else {
					df.sub_total = parseFloat(df.qty) * parseFloat(df.srp)
				}
				cur_frm.refresh_field("promotions")

				var total_qty = compute_promotions_total(frm)
				compute_operations_total(frm,total_qty)
			}
		})
	}
}
cur_frm.cscript.selling = function (frm,cdt,cdn) {
	var df = locals[cdt][cdn]
	if(df.selling){
		frappe.call({
			method: "gaisano_promotions.gaisano_promotions.doctype.promotions_calculation.promotions_calculation.get_promo_item",
			args: {
				promo_item: df,
				supplier: cur_frm.doc.supplier ? cur_frm.doc.supplier : 0
			},
			callback: function (r) {
				df.qty = parseInt(df.warehouse,10) * parseInt(r.message[0].packing,10) + parseInt(df.selling)
				if(cur_frm.doc.promotion_scheme === "Rebate"){
					df.sub_total = parseFloat(df.qty) * parseFloat(df.rebates)
				} else if(cur_frm.doc.promotion_scheme === "Suki Points"){
					df.sub_total = parseFloat(df.qty) * parseFloat(df.points)
				} else {
					df.sub_total = parseFloat(df.qty) * parseFloat(df.srp)
				}
				cur_frm.refresh_field("promotions")

				var total_qty = compute_promotions_total(frm)
				compute_operations_total(frm,total_qty)
			}
		})

	}
}
cur_frm.cscript.packing_per_piece = function (frm,cdt,cdn) {
	var df = locals[cdt][cdn]
	var reb_val = (Math.round(100 * (parseFloat(df.srp) / parseFloat(df.packing_per_piece))) /100 ).toFixed(2).toString()


	if(parseInt(reb_val[reb_val.length - 1]) > 5){
		if(parseInt(reb_val[reb_val.length - 2]) !== 9){
			var increment_number = parseInt(reb_val[reb_val.length - 2]) + 1
			var changed_reb_value = reb_val.slice(0, reb_val.length-2) + increment_number.toString() + "0"
			reb_val = parseFloat(changed_reb_value)
		} else {
			reb_val = Math.ceil(parseFloat(reb_val))
		}
	} else if (parseInt(reb_val[reb_val.length - 1]) < 5 && parseInt(reb_val[reb_val.length - 1]) > 0) {

		reb_val = parseFloat(reb_val.substring(0, reb_val.length - 1) + "5")
	}

		df.srp = reb_val
		df.sub_total = parseFloat(df.srp) * parseFloat(df.qty)
	cur_frm.refresh_field("promotions")
	var total_qty = compute_promotions_total(frm)
		compute_operations_total(frm,total_qty)
}


cur_frm.cscript.purchase_order = function (frm) {
	if(cur_frm.doc.purchase_order){
		cur_frm.doc.promotions_subtotal = 0
		cur_frm.refresh_field("promotions_subtotal")
	} else {
		var total_qty = compute_promotions_total(frm)
		compute_operations_total(frm,total_qty)
	}
}

cur_frm.cscript.points = function (frm,cdt,cdn) {
	var df = locals[cdt][cdn]

	df.sub_total = parseFloat(df.points) * parseFloat(df.qty)

	var total_qty = compute_promotions_total(frm)
	compute_operations_total(frm,total_qty)
}


function compute_promotions_total(frm){
	var total_qty = 0
	var total_promotions_subtotal = 0

	if(frm.promotions !== undefined){
		for(var ii=0;ii<frm.promotions.length;ii+=1){
			total_qty += parseFloat(frm.promotions[ii].qty)
			cur_frm.doc.promotions_subtotal =  parseFloat(cur_frm.doc.promotions_subtotal) + parseFloat(frm.promotions[ii].sub_total)
			cur_frm.refresh_field("promotions_subtotal")
		}
	}

	return total_qty
}

function compute_operations_total(frm,total_qty){
	var total_costs = 0
	if(frm.operations !== undefined){
		for(var x=0;x<frm.operations.length;x+=1){
			frm.operations[x].sub_total = parseFloat(frm.operations[x].operating_cost) * parseFloat(total_qty)
			cur_frm.refresh_field("operations")
			total_costs += frm.operations[x].sub_total
		}
		cur_frm.doc.promo_costs_subtotal = parseFloat(total_costs)
		cur_frm.refresh_field("promo_costs_subtotal")
	}
}
function compute_rebates_items(cur_frm) {
	if(cur_frm.doc.promotions !== undefined){
		for(var i=0;i<cur_frm.doc.promotions.length;i+=1){
			if(cur_frm.doc.percentage){
				var reb_val = (Math.round(100 * (parseFloat(cur_frm.doc.rebate_value)/100) * parseFloat(cur_frm.doc.promotions[i].srp)) /100 ).toFixed(2).toString()

				if(parseInt(reb_val[reb_val.length - 1]) > 5){
					if(parseInt(reb_val[reb_val.length - 2]) !== 9){
						var increment_number = parseInt(reb_val[reb_val.length - 2]) + 1
						var changed_reb_value = reb_val.slice(0, reb_val.length-2) + increment_number.toString() + "0"
						reb_val = parseFloat(changed_reb_value)
					} else {
						reb_val = Math.ceil(parseFloat(reb_val))
					}
				} else if (parseInt(reb_val[reb_val.length - 1]) < 5 && parseInt(reb_val[reb_val.length - 1]) > 0) {

					reb_val = parseFloat(reb_val.substring(0, reb_val.length - 1) + "5")
				}
				cur_frm.doc.promotions[i].rebates = parseFloat(reb_val)
				cur_frm.doc.promotions[i].sub_total = reb_val * parseFloat(cur_frm.doc.promotions[i].qty)
			} else {

				cur_frm.doc.promotions[i].rebates = parseFloat(cur_frm.doc.rebate_value) ? parseFloat(cur_frm.doc.rebate_value) : 0
				cur_frm.doc.promotions[i].sub_total = parseFloat(cur_frm.doc.rebate_value) ? parseFloat(cur_frm.doc.rebate_value) * parseFloat(cur_frm.doc.promotions[i].qty) : 0
			}
		}
		cur_frm.refresh_field("promotions")
	}
}
// function compute_rebates_other_items(cur_frm) {
// 	if(cur_frm.doc.other_items_table !== undefined && cur_frm.doc.rebate_value){
// 		for(var i=0;i<cur_frm.doc.other_items_table.length;i+=1){
// 			if(cur_frm.doc.percentage){
// 				var reb_val = (Math.round(100 * (parseFloat(cur_frm.doc.rebate_value)/100) * parseFloat(cur_frm.doc.other_items_table[i].other_srp)) /100 ).toFixed(2).toString()
//
// 				if(parseInt(reb_val[reb_val.length - 1]) > 5){
// 					if(parseInt(reb_val[reb_val.length - 2]) !== 9){
// 						var increment_number = parseInt(reb_val[reb_val.length - 2]) + 1
// 						var changed_reb_value = reb_val.slice(0, reb_val.length-2) + increment_number.toString() + "0"
// 						reb_val = parseFloat(changed_reb_value)
// 					} else {
// 						reb_val = Math.ceil(parseFloat(reb_val))
// 					}
// 				} else if (parseInt(reb_val[reb_val.length - 1]) < 5 && parseInt(reb_val[reb_val.length - 1]) > 0) {
//
// 					reb_val = parseFloat(reb_val.substring(0, reb_val.length - 1) + "5")
// 				}
// 				cur_frm.doc.other_items_table[i].other_rebates = parseFloat(reb_val)
// 				cur_frm.doc.other_items_table[i].other_sub_total = reb_val * parseFloat(cur_frm.doc.other_items_table[i].other_qty)
// 			} else {
//
// 				cur_frm.doc.other_items_table[i].other_rebates = parseFloat(cur_frm.doc.rebate_value) ?  parseFloat(cur_frm.doc.rebate_value) : 0
// 				cur_frm.doc.other_items_table[i].other_sub_total = parseFloat(cur_frm.doc.rebate_value) ? parseFloat(cur_frm.doc.rebate_value)  * parseFloat(cur_frm.doc.other_items_table[i].other_qty) : 0
// 			}
// 		}
// 		cur_frm.refresh_field("other_items_table")
// 	}
// }