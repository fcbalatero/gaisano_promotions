# -*- coding: utf-8 -*-
# Copyright (c) 2019, bai and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe, json
from frappe.model.document import Document

class PromotionsCalculation(Document):
	def validate(self):
		subtotal = 0.00
		if not self.edit_subtotal and not self.purchase_order and not self.receiving_report and not self.purchase_invoice:
			for x in range(0, len(self.promotions)):
				subtotal += float(self.promotions[x].sub_total)
		promo_costs_subtotal_total = 0.00
		for ii in range(0, len(self.operations)):
			promo_costs_subtotal_total += float(self.operations[ii].sub_total)
		self.promo_costs_subtotal = promo_costs_subtotal_total
		self.promotions_subtotal = subtotal
		self.total_costs = float(self.promotions_subtotal) + float(self.promo_costs_subtotal) + float(self.cellophane_subtotal) + float(self.scotch_tape_subtotal) + float(self.sealer_subtotal)
		total = 0
		for i in range (0,len(self.promotions)):
			total = total + (int(self.promotions[i].qty) / int(self.promotions[i].free_item_qty))
		self.total_qty = total



@frappe.whitelist()
def get_promo_item(promo_item,supplier):
	if supplier:
		data_promo = json.loads(promo_item.decode("utf-8")) if (promo_item.decode('utf-8')) else {}
		if 'promo_item' in data_promo:
			data = frappe.db.sql(""" SELECT * FROM `tabItem` WHERE item_code=%s """,(data_promo['promo_item']),as_dict=True)
			get_supplier = frappe.db.sql(""" SELECT * FROM `tabItem Supplier` WHERE parent=%s and supplier = %s""",(data_promo['promo_item'], supplier),as_dict=True)
			if supplier == get_supplier[0].supplier:
				return data
			else:
				frappe.throw("Item does not belong to the supplier")
	else:
		frappe.throw("No supplier selected")
# @frappe.whitelist()
# def get_other_item(promo_item):
# 	data_promo = json.loads(promo_item.decode("utf-8")) if (promo_item.decode('utf-8')) else {}
# 	if 'other_name' in data_promo:
# 		data = frappe.db.sql(""" SELECT * FROM `tabOther Items` WHERE item_name=%s """,(data_promo['other_name']),as_dict=True)
# 		uom_data =frappe.db.sql(""" SELECT uom FROM `tabUOM Conversion Detail` WHERE parent=%s """,data_promo['other_name'])
# 		return data, uom_data

@frappe.whitelist()
def get_operation(operation):
	data_operation = json.loads(operation.decode("utf-8")) if (operation.decode('utf-8')) else {}
	if 'operation' in data_operation:
		data = frappe.db.sql(""" SELECT * FROM `tabProduction Costs` WHERE name=%s """,(data_operation['operation']),as_dict=True)

		return data

@frappe.whitelist()
def get_operation_sealer():

	data = frappe.db.sql(""" SELECT * FROM `tabProduction Costs` WHERE name=%s """,("Sealer"),as_dict=True)

	return data
@frappe.whitelist()
def get_cellophane_price(cellophane):

	data = frappe.db.sql(""" SELECT * FROM `tabCellophane` WHERE name=%s """,(cellophane),as_dict=True)

	return data

@frappe.whitelist()
def get_scotchtape_price(scotch_tape):

	data = frappe.db.sql(""" SELECT * FROM `tabScotch Tape` WHERE name=%s """,(scotch_tape),as_dict=True)

	return data

@frappe.whitelist()
def get_branch_code(branch):
    branch_code = frappe.db.sql(""" SELECT * FROM `tabBranch` WHERE name=%s """,(branch), as_dict=True)
    return branch_code
