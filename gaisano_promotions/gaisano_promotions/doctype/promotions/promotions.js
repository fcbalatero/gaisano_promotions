// Copyright (c) 2018, bai and contributors
// For license information, please see license.txt

frappe.ui.form.on('Promotions', {
	onload: function(frm) {
		 var bundlesrp = 0
		cur_frm.fields_dict['existing_bundle_item'].get_query = function(doc) {
			return {
				filters: [
					["type", "in", ["Company Bundling", "Promo Pack"]]
				]
			}
		}
		if(!cur_frm.doc.purchase_invoice && !cur_frm.doc.receiving_report && !cur_frm.doc.purchase_order){
			cur_frm.fields_dict['sb_po'].collapse();
		}
		if(cur_frm.doc.name.includes("New Promotions")){
			cur_frm.doc.pcs_in_pack = 1
			cur_frm.refresh_field("pcs_in_pack");
		}
		if(cur_frm.doc.use_existing_bundle){
			cur_frm.set_df_property('existing_bundle_item', 'hidden', 0);
		}
			if(cur_frm.doc.promotion_scheme === "Rebate" || cur_frm.doc.promotion_scheme === "Buy x Take x + Rebate" || cur_frm.doc.promotion_scheme === "Ka Negosyo + Rebate"){
				cur_frm.set_df_property('rebate', 'hidden', 0);
				cur_frm.set_df_property('percentage', 'hidden', 0);
				cur_frm.set_df_property('uom', 'hidden', 0);
			}
			if(frm.item_table !== undefined){
				for(var v=0;v<frm.item_table.length;v++){
					if (frm.item_table[v].free_item !== 1 && frm.item_table[v].free_item !== null){

						var price = frm.item_table[v].price_rate;
						var qty = frm.item_table[v].quantity_pitem;
						bundlesrp += (qty * price);


					} else {
						if(frm.item_table[v].promo_item){
							frappe.call({
						method: "gaisano_promotions.gaisano_promotions.doctype.promotions.promotions.get_possible_uom",
						args: {
							"free_item": frm.item_table[v].promo_item
						},
						callback: function (r) {
							cur_frm.set_df_property('uom', 'options', r.message);
						}
					})
						}

					}
				}
				if(cur_frm.doc.percentage){
					cur_frm.doc.srp_of_bundle = parseFloat(bundlesrp) - (parseFloat(bundlesrp) * (parseFloat(cur_frm.doc.rebate) / 100))
					cur_frm.refresh_field("srp_of_bundle");


				} else {
					console.log("test")
					cur_frm.doc.srp_of_bundle = parseFloat(bundlesrp) - parseFloat(cur_frm.doc.rebate)
					cur_frm.refresh_field("srp_of_bundle");
				}
			}
				cur_frm.refresh_fields();

    },

    refresh: function(frm) {
		if (cur_frm.doc.docstatus === 1){
			frm.add_custom_button(__('Promo Production'), function() {

				frappe.route_options = {
					promo_name: cur_frm.doc.name
				};

				frappe.new_doc('Promo Production');




			}, __('Make'));
			frm.page.set_inner_btn_group_as_primary(__("Make"));
		}

		/*frm.add_fetch('customer', 'tax_id', 'tax_id');

		// formatter for material request item
		frm.set_indicator_formatter('item_code',
			function(doc) { return (doc.stock_qty<=doc.delivered_qty) ? "green" : "orange" })*/
		cur_frm.fields_dict['debit_memo'].get_query = function(doc) {
			return {
				filters: [
					["budget_balance", ">", 0]
				]
			}
		}
	}


});

cur_frm.cscript.edit_srp=function (frm) {
	if(cur_frm.doc.edit_srp){
		cur_frm.set_df_property('srp_of_bundle', 'read_only', 0);
		cur_frm.refresh_field("srp_of_bundle");
	} else {
		cur_frm.set_df_property('srp_of_bundle', 'read_only', 1);
		cur_frm.refresh_field("srp_of_bundle");
	}
}

// cur_frm.cscript.other_name = function (frm,cdt,cdn) {
// 	var df = locals[cdt][cdn]
// 	frappe.call({
// 		method: "gaisano_promotions.gaisano_promotions.doctype.promotions_calculation.promotions_calculation.get_other_item",
// 		args: {
// 			promo_item: df
// 		},
//
// 		callback: function (r) {
// 			df.other_price_rate = r.message[0][0].srp
// 			df.other_qty = 1
// 			cur_frm.refresh_field("promotions_other_items")
// 			cur_frm.set_df_property('uom', 'options', r.message[1]);
// 		}
// 	})
// }
cur_frm.cscript.promo_item=function (frm) {
	get_rate(frm)
}
function get_rate(frm) {

    console.log("AHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
	frappe.call({
		method: "gaisano_promotions.gaisano_promotions.doctype.promotions.promotions.price_rate",

		args: {
			tab: frm.item_table,
			promotions_scheme: frm.promotion_scheme,
			budget: frm.debit_memo
		},

		callback: function (r) {
			var bundlesrp = 0;
			var bundlesrp1 = 0;
			console.log(r.message);
			for (var v = 0; v < frm.item_table.length; v++) {

				frm.item_table[v].price_rate = r.message[0][v];
				frm.item_table[v].description = r.message[1][v];
				if (frm.item_table[v].quantity_pitem === undefined){
                    frm.item_table[v].quantity_pitem = 1;
                }
				cur_frm.refresh_field("item_table");


				if (cur_frm.doc.promotion_scheme === "Rebate") {
					var price = frm.item_table[v].price_rate;
					var qty = frm.item_table[v].quantity_pitem;
					var discount = 0;
					if(frm.percentage ==1){
						discount = 1.00-parseFloat(frm.rebate)/100;
						bundlesrp += qty * price * discount;
					}
					else{
						discount = frm.rebate;
						bundlesrp += qty * (price - discount);
					}

					console.log(bundlesrp);
					frappe.call({
						method: "gaisano_promotions.gaisano_promotions.doctype.promotions.promotions.get_possible_uom",
						args: {
							"free_item": frm.item_table[frm.item_table.length-1].promo_item
						},
						callback: function (r) {
							cur_frm.set_df_property('uom', 'options', r.message);
						}
					})
				}
				else if (cur_frm.doc.promotion_scheme === "Buy x Take x" || cur_frm.doc.promotion_scheme === "Buy x Take x + Rebate"){


					if (frm.item_table[v].free_item !== 1 && frm.item_table[v].free_item !== null){

						var price1 = frm.item_table[v].price_rate;
						var qty1 = frm.item_table[v].quantity_pitem;
						bundlesrp += qty1 * price1;

					} else {
						frappe.call({
							method: "gaisano_promotions.gaisano_promotions.doctype.promotions.promotions.get_possible_uom",
							args: {
								"free_item": frm.item_table[frm.item_table.length-1].promo_item
							},
							callback: function (r) {
								cur_frm.set_df_property('uom', 'options', r.message);
							}
						})
					}

				}
				else if (cur_frm.doc.promotion_scheme === "Ka Negosyo" || cur_frm.doc.promotion_scheme === "Ka Negosyo + Rebate"){
					var price = frm.item_table[v].price_rate;
					var qty = frm.item_table[v].quantity_pitem;
					bundlesrp += qty * price;
					frappe.call({
							method: "gaisano_promotions.gaisano_promotions.doctype.promotions.promotions.get_possible_uom",
							args: {
								"free_item": frm.item_table[frm.item_table.length-1].promo_item
							},
							callback: function (r) {
								cur_frm.set_df_property('uom', 'options', r.message);
							}
					})
				} else if (cur_frm.doc.promotion_scheme === "Suki Points"){
					var price = frm.item_table[v].price_rate;
					var qty = frm.item_table[v].quantity_pitem;
					bundlesrp += qty * price;
					frappe.call({
							method: "gaisano_promotions.gaisano_promotions.doctype.promotions.promotions.get_possible_uom",
							args: {
								"free_item": frm.item_table[frm.item_table.length-1].promo_item
							},
							callback: function (r) {
								cur_frm.set_df_property('uom', 'options', r.message);
							}
					})
				}


			}

			cur_frm.doc.srp_of_bundle = parseFloat(bundlesrp).toFixed(2);
			cur_frm.refresh_field("srp_of_bundle");


		}


	});

};

cur_frm.cscript.quantity_pitem=function (frm) {

	if (cur_frm.doc.promotion_scheme === "Rebate"){
		var total = 0;
		for(var v=0;v<frm.item_table.length;v++){

			var price = frm.item_table[v].price_rate;
			var qty = frm.item_table[v].quantity_pitem;
			var bundlesrp = qty * price;
			total += bundlesrp;

		}

		cur_frm.doc.srp_of_bundle = total;
		cur_frm.refresh_field("srp_of_bundle");
	}
	else if (cur_frm.doc.promotion_scheme === "Buy x Take x" || cur_frm.doc.promotion_scheme === "Buy x Take x + Rebate"){
		var total1 = 0;
		for(var x=0;x<frm.item_table.length;x++){
			if (frm.item_table[x].free_item !== 1 && frm.item_table[x].free_item !== null){

				var price1 = frm.item_table[x].price_rate;
				var qty1 = frm.item_table[x].quantity_pitem;
				var bundlesrp1 = qty1 * price1;
				total1 += bundlesrp1;

			}
			cur_frm.doc.srp_of_bundle = total1;
			cur_frm.refresh_field("srp_of_bundle");
		}

	} else if (cur_frm.doc.promotion_scheme === "Suki Points"){
			if(cur_frm.doc.item_table !== undefined) {
                frappe.call({
                    method: "gaisano_promotions.gaisano_promotions.doctype.promotions.promotions.get_possible_uom",
                    args: {
                        "free_item": frm.item_table[frm.item_table.length - 1].promo_item
                    },
                    callback: function (r) {
                        cur_frm.set_df_property('uom', 'options', r.message);
                    }
                })
            }
	}

	for (var y=0;y<frm.item_table.length;y++){
		if (frm.item_table[y].free_item === 1 && frm.item_table[y].free_item !== undefined && cur_frm.doc.quantity !== undefined && cur_frm.doc.total !== undefined){
			var qty_free = frm.item_table[y].quantity_pitem;
			var tot = cur_frm.doc.total;

			cur_frm.doc.allocation_budget = tot / qty_free;
			cur_frm.doc.total_promo_bundle = tot / qty_free;
			cur_frm.refresh_fields();
		}
	}
};

cur_frm.cscript.free_item=function (frm,cdt,cdn){
	var d = locals[cdt][cdn]
	if((cur_frm.doc.promotion_scheme === "Ka Negosyo" || cur_frm.doc.promotion_scheme === "Ka Negosyo + Rebate") && d.free_item === 1){
		d.price_rate = 0
	} else if((cur_frm.doc.promotion_scheme === "Ka Negosyo" || cur_frm.doc.promotion_scheme === "Ka Negosyo + Rebate") && d.free_item === 0)  {
		frappe.call({
			method: "gaisano_promotions.gaisano_promotions.doctype.promotions.promotions.price_rate",
			args: {
				tab: frm.item_table,
				promotions_scheme: frm.promotion_scheme
			},

			callback: function (r) {
				d.price_rate = r.message[0][0]
								cur_frm.refresh_field("item_table");

			}
		})
	}
	var bundlesrp = 0;
	if(frm.item_table !== undefined){
		for(var v=0;v<frm.item_table.length;v++){
			if (frm.item_table[v].free_item !== 1 && frm.item_table[v].free_item !== null){

				var price = frm.item_table[v].price_rate;
				var qty = frm.item_table[v].quantity_pitem;
				bundlesrp += qty * price;

			} else {
				if(frm.item_table[v].promo_item){
					frappe.call({
				method: "gaisano_promotions.gaisano_promotions.doctype.promotions.promotions.get_possible_uom",
				args: {
					"free_item": frm.item_table[v].promo_item
				},
				callback: function (r) {
					cur_frm.set_df_property('uom', 'options', r.message);
				}
			})
				}

			}
		}
	}

	cur_frm.doc.srp_of_bundle = bundlesrp;
	cur_frm.refresh_field("srp_of_bundle");

	cur_frm.doc.uom = "";
	cur_frm.doc.quantity = " ";
	cur_frm.doc.total = "0";
	cur_frm.doc.allocation_budget = " ";
	cur_frm.doc.total_promo_bundle = "0";
	cur_frm.refresh_fields();



};

cur_frm.cscript.promotion_scheme=function (frm){

	cur_frm.doc.srp_of_bundle = 0;

	var df = frappe.meta.get_docfield("Promo Items", "free_item", cur_frm.doc.name);

	if (cur_frm.doc.promotion_scheme === "Buy x Take x"){
		cur_frm.set_df_property('rebate', 'hidden', 1);
		cur_frm.refresh_field("rebate");

		cur_frm.set_df_property('percentage', 'hidden', 1);
		cur_frm.refresh_field("percentage");

		cur_frm.set_df_property('total', 'read_only', 1);
		cur_frm.refresh_field("total");

		cur_frm.set_df_property('suki_points', 'hidden', 1);
		cur_frm.refresh_field("suki_points");

		df.read_only = 0;
		cur_frm.refresh_field("item_table");
	} else if (cur_frm.doc.promotion_scheme === "Rebate"){

		cur_frm.set_df_property('total', 'read_only', 0);
		cur_frm.refresh_field("total");

		cur_frm.set_df_property('rebate', 'hidden', 0);
		cur_frm.refresh_field("rebate");

		cur_frm.set_df_property('percentage', 'hidden', 0);
		cur_frm.refresh_field("percentage");

		cur_frm.set_df_property('suki_points1', 'hidden', 1);
		cur_frm.refresh_field("suki_points");

		cur_frm.doc.srp_of_bundle = 0;
		cur_frm.refresh_field("srp_of_bundle");

		df.read_only = 1;

		if(frm.item_table !== undefined){
			for (var v = 0; v < frm.item_table.length; v++) {
				frm.item_table[v].free_item = 0;
				cur_frm.refresh_fields("item_table");
			}
		}
	} else if (cur_frm.doc.promotion_scheme === "Suki Points"){
		cur_frm.set_df_property('suki_points', 'hidden', 0);
		cur_frm.refresh_field("suki_points");

		cur_frm.set_df_property('rebate', 'hidden', 1);
		cur_frm.refresh_field("rebate");

		cur_frm.set_df_property('percentage', 'hidden', 1);
		cur_frm.refresh_field("percentage");

		df.read_only = 1;

		if(frm.item_table !== undefined){
			frappe.call({
							method: "gaisano_promotions.gaisano_promotions.doctype.promotions.promotions.get_possible_uom",
							args: {
								"free_item": frm.item_table[frm.item_table.length-1].promo_item
							},
							callback: function (r) {
								cur_frm.set_df_property('uom', 'options', r.message);
							}
					})
			for (var v = 0; v < frm.item_table.length; v++) {
				frm.item_table[v].free_item = 0;
				cur_frm.refresh_fields("item_table");
			}
		}
	} else if (cur_frm.doc.promotion_scheme === "Ka Negosyo"){

		cur_frm.set_df_property('rebate', 'hidden', 1);
		cur_frm.refresh_field("rebate");

		cur_frm.set_df_property('percentage', 'hidden', 1);
		cur_frm.refresh_field("percentage");

		cur_frm.set_df_property('total', 'read_only', 1);
		cur_frm.refresh_field("total");

		cur_frm.set_df_property('suki_points', 'hidden', 1);
		cur_frm.refresh_field("suki_points");

		df.read_only = 0;

		if(frm.item_table !== undefined){
			for (var v = 0; v < frm.item_table.length; v++) {
				frm.item_table[v].free_item = 0;
				cur_frm.refresh_fields("item_table");
			}
		}

	} else if (cur_frm.doc.promotion_scheme === "Buy x Take x + Rebate" || cur_frm.doc.promotion_scheme === "Ka Negosyo + Rebate"){

		cur_frm.set_df_property('rebate', 'hidden', 0);
		cur_frm.refresh_field("rebate");

		cur_frm.set_df_property('percentage', 'hidden', 0);
		cur_frm.refresh_field("percentage");

		cur_frm.set_df_property('total', 'read_only', 1);
		cur_frm.refresh_field("total");

		cur_frm.set_df_property('suki_points', 'hidden', 1);
		cur_frm.refresh_field("suki_points");

		df.read_only = 0;

		if(frm.item_table !== undefined){
			for (var v = 0; v < frm.item_table.length; v++) {
				frm.item_table[v].free_item = 0;
				cur_frm.refresh_fields("item_table");
			}
		}

	}

};

cur_frm.cscript.rebate=function (frm){
	var total = 0;
	var rebate_value = frm.rebate
	if(cur_frm.doc.promotion_scheme === "Rebate"){
		for(var v=0;v<frm.item_table.length;v++){

			var price1 = frm.item_table[v].price_rate;
			var qty1 = frm.item_table[v].quantity_pitem;
			total += qty1 * price1;
		}
	} else if(cur_frm.doc.promotion_scheme === "Buy x Take x + Rebate" || cur_frm.doc.promotion_scheme === "Ka Negosyo + Rebate"){
		for(var vv=0;vv<frm.item_table.length;vv++){
			if(!frm.item_table[vv].free_item){
				var price = frm.item_table[vv].price_rate;
				var qty = frm.item_table[vv].quantity_pitem;
				total += qty * price;
			}
		}
	}

	if(frm.percentage === 1){
		var initial_value = (Math.round(1000 * ((parseFloat(frm.rebate) / 100) * (parseFloat(total)))).toFixed(2) /1000).toFixed(2)
		if(parseInt(initial_value[initial_value.length - 1]) > 5){
			if(parseInt(initial_value[initial_value.length - 2]) !== 9){
				var increment_number_initial = parseInt(initial_value[initial_value.length - 2]) + 1
				var changed_reb_value_initial = initial_value.slice(0, initial_value.length-2) + increment_number_initial.toString() + "0"
				initial_value = parseFloat(changed_reb_value_initial)
			} else {
				initial_value = Math.ceil(parseFloat(initial_value))
			}
		} else if (parseInt(initial_value[initial_value.length - 1]) < 5 && parseInt(initial_value[initial_value.length - 1]) > 0) {
			initial_value = parseFloat(initial_value.replace(initial_value[initial_value.length - 1],"5"))
		}
		rebate_value = parseFloat(parseFloat(total) - parseFloat(initial_value))
		rebate_value = (Math.round(100 * parseFloat(rebate_value)) / 100).toFixed(2).toString()

		if(parseInt(rebate_value[rebate_value.length - 1]) > 5){
			if(parseInt(rebate_value[rebate_value.length - 2]) !== 9){
				var increment_number = parseInt(rebate_value[rebate_value.length - 2]) + 1
				var changed_reb_value = rebate_value.slice(0, rebate_value.length-2) + increment_number.toString() + "0"
				rebate_value = parseFloat(changed_reb_value)
			} else {
				rebate_value = Math.ceil(parseFloat(rebate_value))
			}
		} else if (parseInt(rebate_value[rebate_value.length - 1]) < 5 && parseInt(rebate_value[rebate_value.length - 1]) > 0) {
			rebate_value = parseFloat(rebate_value.replace(rebate_value[rebate_value.length - 1],"5"))
		}
	}
	if(frm.percentage === 1){
		cur_frm.doc.srp_of_bundle = parseFloat(rebate_value).toFixed(2)
	} else {
		cur_frm.doc.srp_of_bundle = parseFloat(total) - parseFloat(rebate_value);
	}
	cur_frm.refresh_field("srp_of_bundle");
};


cur_frm.cscript.percentage=function (frm){
	var total = 0;
	var rebate_value = frm.rebate;
	if(cur_frm.doc.promotion_scheme === "Rebate"){
		for(var v=0;v<frm.item_table.length;v++){

			var price1 = frm.item_table[v].price_rate;
			var qty1 = frm.item_table[v].quantity_pitem;
			total += qty1 * price1;
		}
	} else if(cur_frm.doc.promotion_scheme === "Buy x Take x + Rebate" || cur_frm.doc.promotion_scheme === "Ka Negosyo + Rebate"){
		for(var vv=0;vv<frm.item_table.length;vv++){
			if(!frm.item_table[vv].free_item){
				var price = frm.item_table[vv].price_rate;
				var qty = frm.item_table[vv].quantity_pitem;
				total += qty * price;
			}
		}
	}

	if(frm.percentage === 1){
		var initial_value = (Math.round(1000 * ((parseFloat(frm.rebate) / 100) * (parseFloat(total)))).toFixed(2) /1000).toFixed(2)
		if(parseInt(initial_value[initial_value.length - 1]) > 5){
			if(parseInt(initial_value[initial_value.length - 2]) !== 9){
				var increment_number_initial = parseInt(initial_value[initial_value.length - 2]) + 1
				var changed_reb_value_initial = initial_value.slice(0, initial_value.length-2) + increment_number_initial.toString() + "0"
				initial_value = parseFloat(changed_reb_value_initial)
			} else {
				initial_value = Math.ceil(parseFloat(initial_value))
			}
		} else if (parseInt(initial_value[initial_value.length - 1]) < 5 && parseInt(initial_value[initial_value.length - 1]) > 0) {
			initial_value = parseFloat(initial_value.replace(initial_value[initial_value.length - 1],"5"))
		}
		rebate_value = parseFloat(parseFloat(total) - parseFloat(initial_value))
		rebate_value = (Math.round(100 * parseFloat(rebate_value)) / 100).toFixed(2).toString()

		if(parseInt(rebate_value[rebate_value.length - 1]) > 5){
			if(parseInt(rebate_value[rebate_value.length - 2]) !== 9){
				var increment_number = parseInt(rebate_value[rebate_value.length - 2]) + 1
				var changed_reb_value = rebate_value.slice(0, rebate_value.length-2) + increment_number.toString() + "0"
				rebate_value = parseFloat(changed_reb_value)
			} else {
				rebate_value = Math.ceil(parseFloat(rebate_value))
			}
		} else if (parseInt(rebate_value[rebate_value.length - 1]) < 5 && parseInt(rebate_value[rebate_value.length - 1]) > 0) {
			rebate_value = parseFloat(rebate_value.replace(rebate_value[rebate_value.length - 1],"5"))
		}
	}
	if(frm.percentage === 1){
		cur_frm.doc.srp_of_bundle = parseFloat(rebate_value).toFixed(2)
	} else {
		cur_frm.doc.srp_of_bundle = parseFloat(total) - parseFloat(rebate_value);
	}
	cur_frm.refresh_field("srp_of_bundle");
};
cur_frm.cscript.uom=function (frm) {

    if (frm.item_table === undefined) {
        frappe.msgprint("No free item selected.");
    }


    var chosen_uom = cur_frm.doc.uom;


    console.log(chosen_uom);
    var free_item = 0;
    var qty_free = 0;

    	if(cur_frm.doc.promotion_scheme === "Buy x Take x" || cur_frm.doc.promotion_scheme === "Ka Negosyo" || cur_frm.doc.promotion_scheme === "Buy x Take x + Rebate"){

		for(var v=0;v<frm.item_table.length;v++){
				if(frm.item_table[v].free_item === 1){
					free_item = frm.item_table[v].promo_item;
					qty_free = frm.item_table[v].quantity_pitem;
				}

		}
	} else {
		free_item = frm.item_table[0].promo_item
			qty_free = frm.item_table[0].quantity_pitem;

	}
	if (frm.uom !== undefined) {
		if (frm.quantity !== undefined) {
			var qty = cur_frm.doc.quantity;
			frappe.call({
				method: "gaisano_promotions.gaisano_promotions.doctype.promotions.promotions.total_uom",

				args: {
					free: free_item,
					uom: chosen_uom
				},

				callback: function (r) {

					//var total_alloc = r.message[0];
					var new_total = qty;// * total_alloc;
					var new_total1 = qty * parseInt(cur_frm.doc.pcs_in_pack);


					cur_frm.doc.total = new_total;
					cur_frm.doc.allocation_budget = new_total1 / qty_free;

					// cur_frm.doc.total_promo_bundle = new_total1 / qty_free;
					cur_frm.refresh_fields();

				}

			});

		}
		else {
			frappe.call({
				method: "gaisano_promotions.gaisano_promotions.doctype.promotions.promotions.total_uom",

				args: {
					free: free_item,
					uom: chosen_uom
				},

				callback: function (r) {

					cur_frm.doc.total = r.message[0];
					cur_frm.refresh_field("total");

				}

			});
		}
	}

	/*
	var total = 0;
	var reb = cur_frm.doc.rebate/100;

	for(var v=0;v<frm.item_table.length;v++){

		var price = frm.item_table[v].price_rate;
		var qty = frm.item_table[v].quantity;
		var bundlesrp = qty * price;
		total += bundlesrp;


	}

	var total_rebate = total - (total*reb);

	cur_frm.doc.srp_of_bundle = total_rebate;
	cur_frm.refresh_field("srp_of_bundle");*/

};

cur_frm.cscript.quantity=function (frm) {
	if(cur_frm.doc.uom !== null){
		var qty = cur_frm.doc.quantity;
	}

	var chosen_uom = cur_frm.doc.uom;



	var free_item = 0;
	var qty_free = 0;
	cur_frm.doc.total_promo_bundle = 0;
	if(cur_frm.doc.promotion_scheme === "Buy x Take x" || cur_frm.doc.promotion_scheme === "Ka Negosyo" || cur_frm.doc.promotion_scheme === "Buy x Take x + Rebate" || cur_frm.doc.promotion_scheme === "Ka Negosyo + Rebate"){
                for(var v=0;v<cur_frm.doc.item_table.length;v++){
						if(cur_frm.doc.item_table[v].free_item){
								free_item = cur_frm.doc.item_table[v].promo_item;
								qty_free += parseFloat(cur_frm.doc.item_table[v].quantity_pitem);
						}
                }
        } else {
                free_item = frm.item_table[0].promo_item
			qty_free += parseFloat(cur_frm.doc.item_table[0].quantity_pitem)

        }
	

	frappe.call({
		method: "gaisano_promotions.gaisano_promotions.doctype.promotions.promotions.total_uom",

		args: {
			free: free_item,
			uom: chosen_uom
		},

		callback: function(r){

			//var total_alloc = r.message[0];
			var new_total = qty;// * total_alloc;
			var new_total1 = qty * parseInt(cur_frm.doc.pcs_in_pack);



			cur_frm.doc.total = new_total;
			cur_frm.doc.allocation_budget = new_total1 / qty_free;
			// cur_frm.doc.total_promo_bundle = new_total1 / qty_free;
			cur_frm.refresh_fields();

		}

	});


};
cur_frm.cscript.pcs_in_pack=function (frm) {
	if(cur_frm.doc.uom !== null){
		var qty = cur_frm.doc.quantity;
	}

	var chosen_uom = cur_frm.doc.uom;



	var free_item = 0;
	var qty_free = 0;
	cur_frm.doc.total_promo_bundle = 0;
	if(cur_frm.doc.promotion_scheme === "Buy x Take x" || cur_frm.doc.promotion_scheme === "Ka Negosyo" || cur_frm.doc.promotion_scheme === "Buy x Take x + Rebate" || cur_frm.doc.promotion_scheme === "Ka Negosyo + Rebate"){
                for(var v=0;v<frm.item_table.length;v++){
                                if(frm.item_table[v].free_item === 1){
                                        free_item = frm.item_table[v].promo_item;
                                        qty_free += frm.item_table[v].quantity_pitem;
                                }

                }
        } else {
                free_item = frm.item_table[0].promo_item
				qty_free += frm.item_table[0].quantity_pitem;

        }


	frappe.call({
		method: "gaisano_promotions.gaisano_promotions.doctype.promotions.promotions.total_uom",

		args: {
			free: free_item,
			uom: chosen_uom
		},

		callback: function(r){

			//var total_alloc = r.message[0];
			var new_total = qty;// * total_alloc;
			var new_total1 = qty * parseInt(cur_frm.doc.pcs_in_pack);



			cur_frm.doc.total = new_total;
			cur_frm.doc.allocation_budget = new_total1 / qty_free;
			// cur_frm.doc.total_promo_bundle = new_total1 / qty_free;
			cur_frm.refresh_fields();

		}

	})


};
cur_frm.cscript.total=function (frm) {
	cur_frm.doc.allocation_budget = cur_frm.doc.total;
	cur_frm.refresh_field("allocation_budget");
};

cur_frm.cscript.debit_memo=function (frm){
	if(cur_frm.doc.debit_memo){
		frappe.call({
			method: "gaisano_promotions.gaisano_promotions.doctype.promotions.promotions.get_promobudget",
			args: {
				"promob": cur_frm.doc.debit_memo
			},

			callback: function(r){
				console.log(r)
				cur_frm.doc.total_budget = r.message[0][0].budget_balance;
				cur_frm.doc.supplier = r.message[0][0].supplier;
				cur_frm.doc.branch_code = r.message[1][0].branch_code;
				cur_frm.doc.purchase_order = r.message[0][0].purchase_order;
				cur_frm.doc.receiving_report = r.message[0][0].receiving_report;
				cur_frm.doc.purchase_invoice = r.message[0][0].purchase_invoice;
				cur_frm.refresh_field("purchase_invoice");
				cur_frm.refresh_field("receiving_report");
				cur_frm.refresh_field("purchase_order");
				cur_frm.refresh_field("branch_code");
				cur_frm.refresh_field("total_budget");
				cur_frm.refresh_field("supplier");
			}
		});
	}
}

cur_frm.cscript.use_existing_bundle = function (frm) {
	cur_frm.set_df_property('existing_bundle_item', 'hidden', !cur_frm.doc.use_existing_bundle);
	cur_frm.refresh_field("existing_bundle_item");

	cur_frm.set_df_property('new_item_name', 'read_only', cur_frm.doc.use_existing_bundle);
	cur_frm.refresh_field("new_item_name");

};

cur_frm.cscript.existing_bundle_item = function (frm) {
	if(cur_frm.doc.existing_bundle_item){
		frappe.call({
			method: "gaisano_promotions.gaisano_promotions.doctype.promotions.promotions.get_existing_promotion",
			args: {
				"existing_bundle": cur_frm.doc.existing_bundle_item
			},
			callback: function(r){
				if(r.message.length > 0){
					for(var i=0;i<r.message.length;i+=1){
						var new_row = cur_frm.add_child("item_table");
                        new_row.promo_item = r.message[i].promo_item;
                        new_row.free_item = r.message[i].free_item;
						cur_frm.refresh_field("item_table");
					}
					get_rate(frm)
				}
			}
		});
		frappe.call({
			method: "gaisano_promotions.gaisano_promotions.doctype.promotions.promotions.get_item_name",
			args: {
				"item_code_value": cur_frm.doc.existing_bundle_item
			},
			callback: function(r){
				cur_frm.doc.new_item_name = r.message[0].item_name_dummy;
				cur_frm.refresh_field("new_item_name")
			}
		})
	}
};

// For item branch discounts

frappe.ui.form.on("Item Branch Discount", "supplier_discount", function (frm, cdt, cdn) {
    var discount_row = locals[cdt][cdn];
    frappe.call({
        method: "frappe.client.get",
        args: {
            "doctype": "Supplier Discounts",
            "name": discount_row.supplier_discount
        },
        callback: function (r) {
            frappe.model.set_value(cdt, cdn, "supplier_discount_name", r.message['supplier_discount_name']);
            frm.refresh_field("item_branch_discounts");
        }
    });
});