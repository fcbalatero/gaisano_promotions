# -*- coding: utf-8 -*-
# Copyright (c) 2018, bai and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import json
from pprint import pprint
from frappe.model.naming import make_autoname
from gaisano.events import create_barcode
from gaisano.events import update_last_barcode_promo
from gaisano_promotions.create_barcode import get_item

class Promotions(Document):
	def validate(self):
		if not self.total_budget:
			frappe.throw("Please Select Valid Budget")
	def before_submit(self):
		srp_value = float(self.srp_of_bundle)
		if not self.use_existing_bundle:
			srp = 0
			desc = []

			warehouse_default = frappe.db.sql(""" SELECT name FROM `tabWarehouse` WHERE name LIKE %s """, ("CDO Warehouse%"),
											  as_dict=True)
			pbundle = frappe.get_doc({
				"doctype": "Promo Bundle Items",
				"item_bundle": self.new_item_name
			})
			regular_item = ""
			smallest_item_cost = ""
			for x in range(0, len(self.item_table)):
				item_data_for_item_cost = frappe.db.sql(""" SELECT * FROM `tabItem` WHERE name=%s """, (self.item_table[x].promo_item),as_dict=True)
				if smallest_item_cost and len(item_data_for_item_cost) > 0:
					if float(str(item_data_for_item_cost[0].actual_retail_rate).replace("%", " ")) < float(str(smallest_item_cost).replace("%", " ")):
						smallest_item_cost = float(str(item_data_for_item_cost[0].actual_retail_rate).replace("%", " "))
				else:
					smallest_item_cost = float(str(item_data_for_item_cost[0].actual_retail_rate).replace("%"," "))
				if self.promotion_scheme == "Buy x Take x":
					if not self.item_table[x].free_item:
						regular_item = self.item_table[x].promo_item
				else:
					regular_item = self.item_table[0].promo_item
				if self.item_table[x].free_item:
					pbundle.append("item_table", {
						"promo_item": self.item_table[x].promo_item,
						"description": self.item_table[x].description,
						"quantity_pitem": self.item_table[x].quantity_pitem,
						"free_item": self.item_table[x].free_item,
						"price_rate": self.item_table[x].price_rate,
					})

				else:
					pbundle.append("item_table", {
						"promo_item": self.item_table[x].promo_item,
						"description": self.item_table[x].description,
						"quantity_pitem": self.item_table[x].quantity_pitem,
						"free_item": 0,
						"price_rate": self.item_table[x].price_rate,
					})
				if (self.item_table[x].quantity_pitem > 1):
					str_desc = self.item_table[x].quantity_pitem + "x" + " " + self.item_table[x].description
					desc.append(str(str_desc))
				else:
					desc.append(str(self.item_table[x].description))

				if (self.item_table[x].free_item == 0):
					srp += (float(self.item_table[x].quantity_pitem)) * (float(self.item_table[x].price_rate))
			item_cost = float(self.srp_of_bundle) / float(float(smallest_item_cost)/100 + 1)
			barcode = create_barcode()
			barcode_label_creation = frappe.get_doc({
				'doctype': 'Barcode Label',
				'barcode': barcode
			})
			barcode_label_creation.insert()
			item_master = frappe.get_doc({
				"doctype": "Item",
				"expense_account": "CDO Main Display Area - GG",
				"item_name_dummy": self.new_item_name,
				"item_code": self.new_item_name,
				"item_short_name": self.new_item_name,
				"uom":self.promo_uom,# "Bundle" if self.promotion_scheme == "Buy x Take x" or self.promotion_scheme == "Ka Negosyo" else "Piece",
				"pos_uom": self.promo_uom, #"bundle" if self.promotion_scheme == "Buy x Take x" or self.promotion_scheme == "Ka Negosyo" else "piece",
				"is_stock_item": 1,
				"stock_uom": self.promo_uom,
				"item_price_retail_with_margin": srp_value,
				"item_price_retail_1": item_cost,
				"item_cost": item_cost,
				"valuation_rate": srp_value,
				"packing": 1,
				"item_cost_with_freight": item_cost,
				"item_category": "Grocery",
				"item_group": "Grocery",
				"default_warehouse": warehouse_default[0].name,
				"type": "Company Bundling",
				"barcode_retial": barcode,
				"regular_item": regular_item,
				"item_price":  srp_value,
				"points":  self.suki_points if self.promotion_scheme == "Suki Points" else 0,
				"all_branches": 0
			})

			promotions_budget_data = frappe.db.sql(""" SELECT supplier,branch_code FROM `tabPromotions Budget` WHERE name=%s """,(self.debit_memo),as_dict=True)
			item_class_each_data = frappe.db.sql(""" SELECT * FROM `tabItem Class Each` WHERE parent=%s""",(regular_item),as_dict=True)
			#item_branch_discounts_data = frappe.db.sql(""" SELECT * FROM `tabItem Branch Discount` WHERE parent=%s""",(regular_item),as_dict=True)

			for discount in self.item_branch_discounts:
				item_master.append("item_branch_discounts", {
					"branch": promotions_budget_data[0].branch_code,
					"supplier_discount": discount.supplier_discount,
					"supplier_discount_name": discount.supplier_discount_name
				})
			item_master.append("supplier_items", {
				"supplier": self.supplier,
			})
			item_master.append("display_palcement", {
				"branch": promotions_budget_data[0].branch_code,
				"display_lane": "Promo Area",
				"shelf_capacity": 1,
			})
			item_master.append("item_classification_1", {
				"category": item_class_each_data[0].category,
				"classification": item_class_each_data[0].classification,
				"sub_class": item_class_each_data[0].sub_class,
			})
			item_master.append("branches", {
				"branch": promotions_budget_data[0].branch_code,
			})
			item_master.append("uoms", {
				"uom": self.promo_uom, #"Bundle" if self.promotion_scheme == "Buy x Take x" or self.promotion_scheme == "Ka Negosyo" else "Piece",
				"conversion_factor": 1.00
			})
			update_last_barcode_promo(barcode)
			pbundle.insert()
			item_master.insert()

			# item_master.append("branch_price_table", {
			# 	"branch": "CDO Main",
			# 	"branch_code": self.branch_code,
			# 	"price_id": str(self.branch_code)+ "-" + str(item_master.name),
			# 	"item_cost_with_freight": srp_value,
			# 	"wholesale_price": srp_value,
			# 	"retail_cost": srp_value,
			# 	"retail_price": srp_value
			# }).save()

			if not self.new_item_code:
				self.new_item_code = item_master.item_code

		else:
			item_master = get_item(self.existing_bundle_item)
			item_master_data = frappe.get_doc("Item", self.existing_bundle_item)
			if not item_master:
				item_master_data.append("display_palcement", {
					"branch": "CDO Main",
					"display_lane": "Promo Area",
					"shelf_capacity": 1,
				}).save()
			frappe.db.set_value("Item", self.existing_bundle_item, "item_cost", srp_value)
			frappe.db.set_value("Item", self.existing_bundle_item, "item_price", srp_value)
			frappe.db.set_value("Item", self.existing_bundle_item, "item_price_retail_1", srp_value)
			frappe.db.set_value("Item", self.existing_bundle_item, "item_price_retail_with_margin", srp_value)
			frappe.db.commit()



@frappe.whitelist()
def price_rate(tab,promotions_scheme, budget):
	data = json.loads(tab.decode("utf-8")) if (tab.decode('utf-8')) else {}

	prices = []
	item_name = []
	budget_doc = frappe.get_doc("Promotions Budget", budget)
	promo_calculation = budget_doc.promotions_calculation

	pricedata = []
	pricedata2 = None
	#frappe.msgprint(promo_calculation)

	for i in data:
		if 'promo_item' in i:
			if promotions_scheme == "Ka Negosyo" or promotions_scheme == "Ka Negosyo + Rebate":
				pricedata = frappe.db.sql("""SELECT item_price,item_name FROM `tabItem` WHERE item_code=%s""", (i['promo_item']))
			else:
				if (promo_calculation!="") and (promo_calculation is not None):
					pricedata = frappe.db.sql("""SELECT promo.srp, item.item_name from `tabItem` item join
												`tabPromotions Calculation Table` promo on promo.promo_item = item.name WHERE
												item.name = %s and promo.parent = %s""", (i['promo_item'],promo_calculation))
				pricedata2 = frappe.db.sql("""SELECT item_price_retail_with_margin,item_name FROM `tabItem` WHERE item_code=%s""",(i['promo_item']))
				# pricedata = frappe.db.sql("""SELECT standard_rate,item_name FROM `tabItem` WHERE item_code=%s""", (i['promo_item']))
			if (len(pricedata)>0):
				for x in pricedata:
					prices.append(x[0])
					item_name.append(x[1])
			if (len(pricedata)<1) and (pricedata2 is not None):
				for x in pricedata2:
					prices.append(x[0])
					item_name.append(x[1])

	return prices, item_name

@frappe.whitelist()
def item_list():

	data = []

	value = frappe.db.sql("""SELECT item_name FROM `tabItem`""")
	for i in value:
		data.append(i[0])

	return data

@frappe.whitelist()
def get_possible_uom(free_item):
	data = frappe.db.sql(""" SELECT uom FROM `tabUOM Conversion Detail` WHERE parent=%s """,(free_item))
	return data
@frappe.whitelist()
def total_uom(free, uom):

	data = []

	value = frappe.db.sql("""SELECT conversion_factor FROM `tabUOM Conversion Detail` WHERE parent='{0}' AND uom='{1}'""".format(free, uom))
	for i in value:
		data.append(i[0])

	return data

@frappe.whitelist()
def get_promobudget(promob):

	val = frappe.db.sql("""SELECT * FROM `tabPromotions Budget` WHERE name=%s""", promob, as_dict=True)
	if val[0].docstatus == 1:
		val_branch = frappe.db.sql("""SELECT * FROM `tabBranch` WHERE name=%s""", val[0].branch_code, as_dict=True)
		return val,val_branch
	else:
		frappe.throw("Please submit selected budget")

@frappe.whitelist()
def get_existing_promotion(existing_bundle):
	array = []
	old_item_name = frappe.db.sql(""" SELECT * FROM tabItem WHERE item_code=%s """,(existing_bundle),as_dict=True)
	old_promo = frappe.db.sql("""SELECT * FROM `tabPromotions` WHERE new_item_name=%s and docstatus=%s""", (old_item_name[0].item_name_dummy,1), as_dict=True)
	if len(old_promo) > 0:
		old_promo_table = frappe.db.sql("""SELECT * FROM `tabPromo Items` WHERE parent=%s""", old_promo[0].name, as_dict=True)
		return old_promo_table
	return array

@frappe.whitelist()
def get_item_name(item_code_value):
	data = frappe.db.sql(""" SELECT * FROM `tabItem` WHERE item_code=%s """,(item_code_value),as_dict=True)
	return data