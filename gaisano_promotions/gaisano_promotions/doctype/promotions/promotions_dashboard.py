from frappe import _

def get_data():
	return {
		'heatmap': False,
		'fieldname': 'promo_name',
		'transactions': [
			{
				'label': _('Promo Production'),
				'items': ['Promo Production']
			}
		]
	}