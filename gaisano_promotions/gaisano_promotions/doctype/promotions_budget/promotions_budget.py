# -*- coding: utf-8 -*-
# Copyright (c) 2018, bai and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class PromotionsBudget(Document):
	def validate(self):
		if self.total_budget <= 0:
			frappe.throw("Total budget must be greater than zero")


