from frappe import _

def get_data():
	return {
		'heatmap': False,
        'fieldname': 'debit_memo',
		'transactions': [
			{
				'label': _('Promo Production'),
				'items': ['Promo Production']
			},
            {
                'label': _('Promotions'),
                'items': ['Promotions']
            }
		]
	}