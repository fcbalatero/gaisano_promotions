# -*- coding: utf-8 -*-
# Copyright (c) 2019, bai and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class PrintBarcode(Document):
	def validate(self):
		# if self.status == "Approved":
		# 	frappe.db.sql(""" UPDATE `tabDocPerm` SET print=%s WHERE role=%s and parent=%s """,(1,"Promotions Manager", "Print Barcode"))
		# 	frappe.db.sql(""" UPDATE `tabDocPerm` SET print=%s WHERE role=%s and parent=%s """,(1,"System Manager", "Print Barcode"))
		# 	frappe.db.sql(""" UPDATE `tabDocPerm` SET print=%s WHERE role=%s and parent=%s """,(1,"Promotions User", "Print Barcode"))
		# 	frappe.db.sql(""" UPDATE `tabDocPerm` SET print=%s WHERE role=%s and parent=%s """,(1,"Barcode Printing Manager", "Print Barcode"))
		# elif self.status == "Draft":
		# 	frappe.db.sql(""" UPDATE `tabDocPerm` SET print=%s WHERE role=%s  and parent=%s""", (0, "Promotions Manager","Print Barcode"))
		# 	frappe.db.sql(""" UPDATE `tabDocPerm` SET print=%s WHERE role=%s  and parent=%s""", (0, "System Manager","Print Barcode"))
		# 	frappe.db.sql(""" UPDATE `tabDocPerm` SET print=%s WHERE role=%s  and parent=%s""", (0, "Promotions User","Print Barcode"))
		# 	frappe.db.sql(""" UPDATE `tabDocPerm` SET print=%s WHERE role=%s  and parent=%s""", (0, "Barcode Printing Manager","Print Barcode"))

		if self.type == "Small": #width: 1.70in, height: 0.30in
			html_code = "<img style='width: 5in; height: 0.40in' src='../files/" + str(self.item_barcode_number) + ".svg' />"
		elif self.type == "Big":
			html_code = "<img style='width: 2.6in; height: 0.8in' src='../files/" + str(self.item_barcode_number) + ".svg' />"

		self.html_code = html_code


@frappe.whitelist()
def get_item(item_name):
	data = frappe.db.sql(""" SELECT * FROM `tabItem` WHERE item_name_dummy=%s""", item_name, as_dict=True)
	return data