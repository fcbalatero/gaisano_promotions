# -*- coding: utf-8 -*-
# Copyright (c) 2018, Bai Web and Mobile Lab and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

import barcode, re
from barcode.writer import ImageWriter, SVGWriter
from frappe.utils.file_manager import get_file_path

class BarcodeLabel(Document):
    def generate_barcode(self):
        """Generate barcode label based from the barcode field"""
        if self.flags.in_insert:
            is_private = 0
            path_private = '/private'
            if is_private == 0:
                path_private = ''

            CODE39 = barcode.get_barcode_class('ean13')
            code39_ = CODE39(self.barcode, writer=MyImageWriter(), add_checksum=False)
            code_ = re.sub(' ', '_', self.barcode)

            x_file = "%s.png" % str(code_).replace('/', '')

            path_code39 = path_private + '/files/' + ("%s" % str(code_).replace('/', ''))
            code39_.save(
                get_file_path(path_code39))  # creates barcode image and saves it in: sites/sitename/public/files folder
            # path = frappe.get_site_path('public', 'files', self.barcode)

            file_name = '{0}.svg'.format(self.barcode)

            file = frappe.get_doc({
                'doctype': 'File',
                'file_name': file_name,
                'folder': 'Home/Attachments',
                'attached_to_name': self.barcode,
                'attached_to_doctype': 'Barcode Label',
                'file_url': '/files/{0}'.format(file_name),
                'is_private': 0
            })

            file.insert()

            self.image = '/files/{0}'.format(file_name)

    def before_save(self):
        """Setup the document"""
        make_barcode_img(self.barcode)
def make_barcode_img(code):
    is_private = 0
    path_private = '/private'
    if is_private == 0:
        path_private = ''

    CODE39 = barcode.get_barcode_class('ean13')
    code39_ = CODE39(code, writer=MyImageWriter())
    code_ = re.sub(' ', '_', code)


    x_file = "%s.png" % str(code_).replace('/','')

    path_code39 = path_private + '/files/' + ("%s" % str(code_).replace('/',''))
    code39_.save(get_file_path(path_code39)) #creates barcode image and saves it in: sites/sitename/public/files folder


    x_path = path_private + '/files/' + x_file
    print path_code39
    return x_path, str(code39_).replace('/','')

"""
To test, try running :
    bench execute gem_manufacturing.barcodetasks.make_barcode_img --args "the string which will be parsed to a barcode";
"""

def mm2px(mm, dpi=300):
    return (mm * dpi) / 25.4


class MyImageWriter(SVGWriter):
    def calculate_size(self, modules_per_line, number_of_lines, dpi=300):
        # self.text_distance = 1
        self.text = None
        self.module_width = 0.2
        # self.quite_zone = 0
        # self.text_distance = 0

        self.module_height = 15.0
        # print "================++++++++++++++++++++=============="
        # print "Module Width"
        # print self.module_width
        width = 1.8 * self.quiet_zone + modules_per_line * self.module_width
        height = 0.7 + self.module_height * number_of_lines
        if self.text:
            height += (self.font_size + self.text_distance) / 3
        else:
            height += 0.7

        return int(mm2px(width, dpi)), int(mm2px(height, dpi))

    def _paint_text(self, xpos, ypos):
        # this should align your font to the left side of the bar code:
        xpos = self.quiet_zone
        pos = (mm2px(xpos, self.dpi), mm2px(ypos, self.dpi))
        self._draw.text(pos, self.text, fill=self.foreground)
    #test_push


#backup
# def generate_barcode(self):
#     """Generate barcode label based from the barcode field"""
#     if self.flags.in_insert:
#         label = barcode.codex.Code39(self.barcode, ImageWriter(), add_checksum=False)
#
#         path = frappe.get_site_path('public', 'files', self.barcode)
#
#         label.save(path)
#
#         file_name = '{0}.png'.format(self.barcode)
#
#         file = frappe.get_doc({
#             'doctype': 'File',
#             'file_name': file_name,
#             'folder': 'Home/Attachments',
#             'attached_to_name': self.barcode,
#             'attached_to_doctype': 'Barcode Label',
#             'file_url': '/files/{0}'.format(file_name),
#             'is_private': 0
#         })
#
#         file.insert()
#
#         self.image = '/files/{0}'.format(file_name)