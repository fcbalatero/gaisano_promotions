# Copyright (c) 2013, bai and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe

def execute(filters=None):
	columns, data = [], []
	columns = [
		{"label": "Promo Name", 'width': 200, "fieldname": "promo_name"},
		{"label": "Promotion Scheme", 'width': 200, "fieldname": "promotion_scheme"},
		{"label": "Promotions Budget", 'width': 200, "fieldname": "promotions_budget"},
		{"label": "Supplier", 'width': 200, "fieldname": "supplier"},
		{"label": "Promo Bundle Name", 'width': 200, "fieldname": "promo_bundle_name"}
	]
	if filters.get("promotions_filter") == "On-going Promotions":
		promotions_data = frappe.db.sql(""" SELECT * FROM `tabPromotions` WHERE total_budget > 0 """, as_dict=True)
		columns.append({"label": "Budget Balance", 'width': 200, "fieldname": "budget_balance"})
		for i in promotions_data:
			data.append({
				'promo_name': i.promo_name or "",
				'promotion_scheme': i.promotion_scheme or "",
				'promotions_budget': i.promotions_budget or "",
				'supplier': i.supplier or "",
				"promo_bundle_name": i.new_item_name or "",
				'budget_balance': i.total_budget
			})
		print(promotions_data)
	elif filters.get("promotions_filter") == "Completed Promotions":
		promotions_data = frappe.db.sql(""" SELECT * FROM `tabPromotions` WHERE total_budget = 0 """, as_dict=True)
		columns.append({"label": "Budget Balance", 'width': 200, "fieldname": "budget_balance"})

		for i in promotions_data:
			data.append({
				'promo_name': i.promo_name or "",
				'promotion_scheme': i.promotion_scheme or "",
				'promotions_budget': i.promotions_budget or "",
				'supplier': i.supplier or "",
				"promo_bundle_name": i.new_item_name or "",
				'budget_balance': i.total_budget

			})
		print(promotions_data)
	return columns, data
