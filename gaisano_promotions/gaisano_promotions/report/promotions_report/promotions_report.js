// Copyright (c) 2016, bai and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Promotions Report"] = {
	"filters": [
		{
            "fieldname": "promotions_filter",
            "label": __("Promotions"),
            "fieldtype": "Select",
            "options": "On-going Promotions\nCompleted Promotions",
			"reqd": 1
        }
	]
}
