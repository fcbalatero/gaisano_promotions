# Copyright (c) 2013, bai and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe

def execute(filters=None):
	columns, data = [], []
	promotion_schemes = ["Buy x Take x", "Rebate", "Suki Points", "Ka Negosyo", "Buy x Take x + Rebate"]
	columns = [
		{"label": "Promotions", 'width': 200, "fieldname": "promotions"},
		{"label": "Promotion Scheme", 'width': 200, "fieldname": "promotions_scheme"},
		{"label": "Promotion Name", 'width': 500, "fieldname": "promotions_name"},
		{"label": "Total Promo Quantity Produced", 'width': 250, "fieldname": "total_quantity"},
		{"label": "Production Amount", 'width': 200, "fieldname": "total_amount"}
	]
	if filters.get("supplier") :
		if filters.get("start_date") and filters.get("end_date"):
			promotions_data = frappe.db.sql(""" SELECT * FROM `tabPromotions` WHERE docstatus=%s and supplier=%s and date BETWEEN %s and %s""", (1,filters.get("supplier"),filters.get("start_date"),filters.get("end_date")),as_dict=True)
		else:
			promotions_data = frappe.db.sql(
				""" SELECT * FROM `tabPromotions` WHERE supplier=%s and docstatus=%s """,
				(filters.get("supplier"),1), as_dict=True)

		overall_total_amount = 0.00
		buyxtakex_total_amount = 0.00
		rebate_total_amount = 0.00
		sukipoints_total_amount = 0.00
		kanegosyo_total_amount = 0.00
		buyxtakexrebate_total_amount = 0.00
		for i in promotions_data:
			promo_production = frappe.db.sql(""" SELECT SUM(total_costs) FROM `tabPromo Production` WHERE promo_name=%s and docstatus=%s""",(i.name,1))
			total_quantity_promo_production = frappe.db.sql(""" SELECT SUM(total_quantity) FROM `tabPromo Production` WHERE promo_name=%s and docstatus=%s""",(i.name,1))
			print(promo_production)
			if len(promo_production) > 0:
				overall_total_amount += float(promo_production[0][0]) if promo_production[0][0] != None else 0
				if i.promotion_scheme == "Buy x Take x":
					buyxtakex_total_amount += float(promo_production[0][0]) if promo_production[0][0] != None else 0
				elif i.promotion_scheme == "Rebate":
					rebate_total_amount += float(promo_production[0][0]) if promo_production[0][0] != None else 0
				elif i.promotion_scheme == "Suki Points":
					sukipoints_total_amount += float(promo_production[0][0]) if promo_production[0][0] != None else 0
				elif i.promotion_scheme == "Ka Negosyo":
					kanegosyo_total_amount += float(promo_production[0][0]) if promo_production[0][0] != None else 0
				elif i.promotion_scheme == "Buy x Take x + Rebate":
					buyxtakexrebate_total_amount += float(promo_production[0][0]) if promo_production[0][0] != None else 0

				data.append({
					'promotions': i.name,
					'promotions_scheme': i.promotion_scheme,
					'promotions_name': i.new_item_name,
					'total_quantity': "{:,}".format(total_quantity_promo_production[0][0]) if total_quantity_promo_production[0][0] != None else 0,
					'total_amount': "{:,}".format(promo_production[0][0]) if promo_production[0][0] != None else 0
				})
		if len(promotions_data) > 0:
			for x in promotion_schemes:
				data.append({
					'total_quantity': x + " Total Amount",
					'total_amount': "{:,}".format(round(buyxtakex_total_amount,2)) if x == "Buy x Take x" else "{:,}".format(round(rebate_total_amount,2)) if x == "Rebate" else "{:,}".format(round(sukipoints_total_amount,2)) if x == "Suki Points" else "{:,}".format(round(kanegosyo_total_amount,2)) if x == "Ka Negosyo"else "{:,}".format(round(buyxtakexrebate_total_amount,2)) if x == "Buy x Take x + Rebate" else 0,
				})
			data.append({
				'total_quantity': "Total Amount",
				'total_amount': "{:,}".format(overall_total_amount),
			})
	else:
		suppliers_data = frappe.db.sql(""" SELECT * FROM `tabSupplier`""", as_dict=True)

		for ii in suppliers_data:
			overall_total_amount = 0.00
			buyxtakex_total_amount = 0.00
			rebate_total_amount = 0.00
			sukipoints_total_amount = 0.00
			kanegosyo_total_amount = 0.00
			buyxtakexrebate_total_amount = 0.00
			if filters.get("start_date") and filters.get("end_date"):
				promotions = frappe.db.sql(""" SELECT * FROM `tabPromotions` WHERE docstatus=%s and supplier=%s and date BETWEEN %s and %s""", (1,ii.supplier_name,filters.get("start_date"),filters.get("end_date")), as_dict=True)
			else:
				promotions = frappe.db.sql(""" SELECT * FROM `tabPromotions` WHERE supplier=%s and docstatus=%s """, (ii.supplier_name,1), as_dict=True)
			if len(promotions) > 0:
				data.append({
					'promotions': "Supplier Name",
					'promotions_scheme': ii.supplier_name,
				})
				for x in promotions:
					promo_production = frappe.db.sql(""" SELECT SUM(total_costs) FROM `tabPromo Production` WHERE promo_name=%s and docstatus=%s""",(x.name,1))
					total_quantity_promo_production = frappe.db.sql(
						""" SELECT SUM(total_quantity) FROM `tabPromo Production` WHERE promo_name=%s and docstatus=%s""",
						(x.name, 1))

					if len(promo_production) > 0:

						overall_total_amount += float(promo_production[0][0]) if promo_production[0][0] != None else 0
						if x.promotion_scheme == "Buy x Take x":
							buyxtakex_total_amount += float(promo_production[0][0]) if promo_production[0][0] != None else 0
						elif x.promotion_scheme == "Rebate":
							rebate_total_amount += float(promo_production[0][0]) if promo_production[0][0] != None else 0
						elif x.promotion_scheme == "Suki Points":
							sukipoints_total_amount += float(promo_production[0][0]) if promo_production[0][0] != None else 0
						elif x.promotion_scheme == "Ka Negosyo":
							kanegosyo_total_amount += float(promo_production[0][0]) if promo_production[0][0] != None else 0
						elif x.promotion_scheme == "Buy x Take x + Rebate":
							buyxtakexrebate_total_amount += float(promo_production[0][0]) if promo_production[0][0] != None else 0

						data.append({
							'promotions': x.name,
							'promotions_scheme': x.promotion_scheme,
							'promotions_name':x.new_item_name,
							'total_quantity': "{:,}".format(total_quantity_promo_production[0][0]) if total_quantity_promo_production[0][0] != None else 0,
							'total_amount': "{:,}".format(promo_production[0][0]) if promo_production[0][0] != None else 0
						})
				if round(buyxtakex_total_amount, 2) > 0 or round(rebate_total_amount, 2) > 0 or round(sukipoints_total_amount, 2) > 0 or round(kanegosyo_total_amount, 2) > 0 or round(buyxtakexrebate_total_amount, 2) > 0:
					for x in promotion_schemes:
						data.append({
							'total_quantity': x + " Total Amount",
							'total_amount': "{:,}".format(
								round(buyxtakex_total_amount, 2)) if x == "Buy x Take x" else "{:,}".format(
								round(rebate_total_amount, 2)) if x == "Rebate" else "{:,}".format(
								round(sukipoints_total_amount, 2)) if x == "Suki Points" else "{:,}".format(
								round(kanegosyo_total_amount, 2)) if x == "Ka Negosyo"else "{:,}".format(
								round(buyxtakexrebate_total_amount, 2)) if x == "Buy x Take x + Rebate" else 0,
						})
					data.append({
						'total_quantity': "Total Amount",
						'total_amount': "{:,}".format(overall_total_amount),
					})
				data.append({
					'promotions': "",
				})
	return columns, data
