# Copyright (c) 2013, bai and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe

def execute(filters=None):
	columns, data = [], []
	columns = [
		{"label": "Promotions Name", 'width': 200, "fieldname": "promo_name"},
		{"label": "Promotions Scheme", 'width': 200, "fieldname": "promotions_scheme"},
		{"label": "Date", 'width': 200, "fieldname": "date"},
		{"label": "Bundle Item Name", 'width': 200, "fieldname": "bundle_item_name"},
		{"label": "Total Budget", 'width': 200, "fieldname": "total_budget"},
		{"label": "Branch Code", 'width': 200, "fieldname": "branch_code"},
		{"label": "SRP of Bundle", 'width': 200, "fieldname": "srp"},
		{"label": "Total Allocation", 'width': 200, "fieldname": "total_allocation"},
		{"label": "Bundle Balance", 'width': 200, "fieldname": "bundle_balance"},
	]
	promotions_under_dm = frappe.db.sql(""" SELECT * FROM `tabPromotions` WHERE debit_memo=%s """,(filters.get("promotions_budget")),as_dict=True)
	if len(promotions_under_dm) > 0:
		for i in promotions_under_dm:
			data.append({
				'promo_name': i.promo_name or "",
				'promotions_scheme': i.promotion_scheme or "",
				'date': i.date or "",
				'bundle_item_name': i.new_item_name or "",
				"total_budget": i.total_budget or "",
				"branch_code": i.branch_code or "",
				"srp": i.srp_of_bundle or "",
				"total_allocation": i.total or "",
				"bundle_balance": i.allocation_budget or "",
			})
	return columns, data
