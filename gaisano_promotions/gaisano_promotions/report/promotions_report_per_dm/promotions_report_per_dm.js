// Copyright (c) 2016, bai and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Promotions Report Per DM"] = {
	"filters": [
		{
            "fieldname": "promotions_budget",
            "label": __("Promotions Budget"),
            "fieldtype": "Link",
            "options": "Promotions Budget",
            "get_query": function() {
                return {
                    "doctype": "Promotions Budget",
                    "filters": {
                        "docstatus": 1,
                    }
                }
            },
			"reqd": 1
        },
	]
}
