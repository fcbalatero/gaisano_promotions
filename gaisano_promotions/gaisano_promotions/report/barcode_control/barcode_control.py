# Copyright (c) 2013, bai and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe

def execute(filters=None):
	columns, data = [], []

	columns = [
		{"label": "Barcode", 'width': 200, "fieldname": "barcode"},
		{"label": "Qty", 'width': 200, "fieldname": "qty"},
		{"label": "Description", 'width': 200, "fieldname": "description"},
		{"label": "Price", 'width': 200, "fieldname": "price"},
		{"label": "Series", 'width': 200, "fieldname": "series"},
		{"label": "Supplier", 'width': 200, "fieldname": "supplier", "hidden": 0},
		{"label": "Debit Memo", 'width': 200, "fieldname": "debit_no", "hidden": 0},
		{"label": "Total Budget", 'width': 200, "fieldname": "total_budget", "hidden": 0},
	]

	promo_production_data = frappe.db.sql(""" SELECT * FROM `tabPromo Production` WHERE debit_memo=%s """,(filters.get("promotions_budget")),as_dict=True)
	promotions_budget_data = frappe.db.sql(""" SELECT * FROM `tabPromotions Budget` WHERE name=%s """,(filters.get("promotions_budget")),as_dict=True)
	if len(promo_production_data) > 0:
		total_qty = 0
		for i in promo_production_data:
			item_barcode = frappe.db.sql(""" SELECT barcode_retial,item_price_retail_with_margin FROM `tabItem` WHERE  item_name_dummy=%s""",(i.promotion_name),as_dict=True)
			data.append({
				'barcode': item_barcode[0].barcode_retial or "",
				'qty': i.total_quantity or "",
				'description': i.promotion_name or "",
				'price': item_barcode[0].item_price_retail_with_margin or "",
				'series': i.name or "",
				'supplier': promotions_budget_data[0].supplier or "",
				'debit_no': promotions_budget_data[0].debit_memo or "",
				'total_budget': promotions_budget_data[0].total_budget or "",
			})
			total_qty += int(i.total_quantity)
		data.append({
			'barcode': "Total Qty",
			'qty': total_qty,
		})

	return columns, data
