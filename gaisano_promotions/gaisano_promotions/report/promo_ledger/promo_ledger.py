# Copyright (c) 2013, bai and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe

def execute(filters=None):
	data = []

	columns = [
		{"label": "Promo Item", 'width': 200, "fieldname": "item"},
		{"label": "Transaction", 'width': 200, "fieldname": "transaction"},
		{"label": "QTY", 'width': 80, "fieldname": "qty", "fieldtype":"Float","precision":2},
		{"label": "QTY after Transaction", 'width': 80, "fieldname": "qty_after_transaction","fieldtype":"Float","precision":2}
	]

	promotions = filters.get("promotion")
	promotions_doc = frappe.get_doc("Promotions", promotions)
	balance = float(promotions_doc.total)
	promo_production = frappe.db.sql("""SELECT * from `tabPromo Production` where docstatus = 1 and promo_name = %s""",
									 promotions, as_dict = True)

	link_str="<a href='#Form/Promotions/"+promotions+"'>"+promotions+"</a>"
	data.append({"item":promotions_doc.new_item_name, "transaction":link_str,"qty":promotions_doc.total, "qty_after_transaction":promotions_doc.total})

	for row in promo_production:
		balance -= float(row.total_quantity)
		link_str = "<a href='#Form/Promo Production/" + row.name + "'>" + row.name + "</a>"
		data.append(
			{"item": promotions_doc.new_item_name, "transaction": link_str, "qty": row.total_quantity,
			 "qty_after_transaction": balance})

	data.append(
		{"item": "ENDING BALANCE", "transaction": "", "qty": None,
		 "qty_after_transaction": balance})
	return columns, data
