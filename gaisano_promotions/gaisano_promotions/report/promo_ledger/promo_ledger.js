// Copyright (c) 2016, bai and contributors
// For license information, please see license.txt

frappe.query_reports["Promo Ledger"] = {
	"filters": [
		{
			"fieldname": "promotion",
            "label": __("Promotion"),
            "fieldtype": "Link",
            "options": "Promotions",
            "get_query": function() {
                return {
                    "doctype": "Promotions",
                    "filters": {
                        "docstatus": 1,
                    }
                }
            },
			"reqd": 1
		}
	]
};
