# -*- coding: utf-8 -*-
# Copyright (c) 2018, Bai Web and Mobile Lab and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe

def generate_barcode(barcode_value):
    barcode_label_creation = frappe.get_doc({
			'doctype': 'Barcode Label',
			'barcode': barcode_value
		})
    barcode_label_creation.insert()

def get_item(item):
	item_master = frappe.get_doc("Item", item)

	if len(item_master.display_palcement) > 0:
		for i in item_master.display_palcement:
			if i.branch == "CDO Main":
				return True
	return False

def check_items():
	promotions_items = frappe.db.sql(""" SELECT * FROM `tabPromotions` WHERE use_existing_bundle=%s AND docstatus=%s AND new_item_code is null""",(0,1),as_dict=True)
	for i in promotions_items:
		item_data = frappe.db.sql(""" SELECT item_code FROM tabItem 
 									  WHERE item_name_dummy=%s AND barcode_retial 
 									  LIKE %s AND (owner=%s OR owner=%s) AND item_price_retail_with_margin = %s """,
								  (i.new_item_name, "601%","aileen@gaisano.com","claudine@gaisano.com", i.srp_of_bundle), as_dict=True)
		if len(item_data) > 0:
			promotions_to_be_update = frappe.get_doc("Promotions", i.name)
			promotions_to_be_update.db_set("new_item_code",item_data[0].item_code)
			promotions_to_be_update.save()
			print("Success updating type for PROMOTIONS - " + i.name)

		else:
			print("No Item Record for PROMOTIONS - " + i.name)

@frappe.whitelist()
def check_role(user):
	data = frappe.db.sql(""" SELECT role FROM `tabUserRole` WHERE parent=%s""",user)
	return data

def test_adding_item():
	from gaisano.events import create_barcode
	from gaisano.events import update_last_barcode_promo
	try:
		barcode = create_barcode()
		barcode_label_creation = frappe.get_doc({
			'doctype': 'Barcode Label',
			'barcode': barcode
		})
		barcode_label_creation.insert()
		item_master = frappe.get_doc({
			"doctype": "Item",
			"item_name_dummy": "test1",
			"item_code": "tests",
			"item_short_name": "test1",
			"uom": "Bundle" ,
			"pos_uom": "bundle" ,
			"is_stock_item": 1,
			"item_price_retail_with_margin": "14.20",
			"item_cost": "14.20",
			"valuation_rate": "14.20",
			"packing": 1,
			"item_cost_with_freight": "14.20",
			"item_category": "Grocery",
			"item_group": "Grocery",
			"default_warehouse": "CDO Warehouse - GG",
			"type": "Company Bundling",
			"barcode_retial": barcode,
			"item_price": "14.20",
			"points": 0,
			"all_branches": 0
		})
		update_last_barcode_promo(barcode)
		item_master.insert()
		item_master.save()
		item_master_get_item_again = frappe.get_doc("Item", item_master.name)

		item_master_get_item_again.append("branch_price_table", {
			"branch": "CDO Main",
			"branch_code": "MALL",
			"price_id": "MALL-" + str(item_master_get_item_again.name),
			"item_cost_with_freight": "14.20",
			"wholesale_price": "14.20",
			"retail_cost": "14.20",
			"retail_price": "14.20"
		}).save()
		print("ITEM CODE")
		print(item_master.item_code)
	except:
		print(frappe.get_traceback())

def regenerate_barcode():
	promotions_data = frappe.db.sql(""" SELECT * FROM `tabBarcode Label`""",as_dict=True)
	for i in promotions_data:
		try:
			frappe.delete_doc("Barcode Label",i.name)
			barcode_label_creation = frappe.get_doc({
				'doctype': 'Barcode Label',
				'barcode': i.name
			})
			barcode_label_creation.insert()
		except:
			print(frappe.get_traceback())

def create_se_for_specific_prod(prod_name):
	promo_prod_doc = frappe.get_doc("Promo Production", prod_name)
	warehouse_default_promo = frappe.db.sql(""" SELECT name FROM `tabWarehouse` WHERE name LIKE %s """, ("Promo%"),
											as_dict=True)

	item_name_in_promotions = frappe.db.sql(""" SELECT * FROM `tabPromotions` WHERE name=%s """, (promo_prod_doc.promo_name),
											as_dict=True)
	item_code_new_item = frappe.db.sql(""" SELECT * FROM `tabItem` WHERE item_name_dummy=%s """,
									   (item_name_in_promotions[0].new_item_name), as_dict=True)
	promo_budget_branch_code = frappe.db.sql(""" SELECT branch_code FROM `tabPromotions Budget` WHERE name=%s """,
											 (promo_prod_doc.debit_memo), as_dict=True)
	material_receipt_doc = frappe.get_doc({
		"doctype": "Stock Entry",
		"type": "Stock Transfer",
		"branch": promo_budget_branch_code[0].branch_code,
		"naming_series": "PROMO-." + promo_prod_doc.branch_code + ".-.YY.MM.DD",
	})
	material_receipt_doc.append("items", {
		"s_warehouse": warehouse_default_promo[0].name,
		"t_warehouse": promo_prod_doc.target_warehouse,
		"item_code": item_code_new_item[0].item_code,
		"qty_dummy": float(promo_prod_doc.total_quantity),
		"qty": float(promo_prod_doc.total_quantity),
		"uom": "Case",
	})
	try:
		material_receipt_doc.insert(ignore_permissions=True)
		frappe.db.sql(""" UPDATE `tabPromo Production` SET stock_entry_name=%s  WHERE name=%s""",
					  (material_receipt_doc.name, promo_prod_doc.name))
		material_receipt_doc.submit()
	except Exception:
		print(frappe.get_traceback())


